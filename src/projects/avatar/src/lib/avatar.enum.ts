import { Component } from '@angular/core';
@Component({
  template: '',
})
export class AvatarOptions {
  style: AvatarStyle;
  top: Top;
  accessories: Accessories;
  hairColor: HairColor;
  facialHair: FacialHair;
  clothes: Clothes;
  clothColor: ClothesColor;
  eyes: Eyes;
  eyebrow: Eyebrow;
  mouth: Mouth;
  skin: Skin;
  hatColor: HatColor;
  facialHairColor: FacialHairColor;
  graphic: Graphic;
  face: Face;

  constructor() {
    // AVATAR DEFAULT
    this.top = Top.NO_HAIR;
    this.face = Face.EYEBROW;
    this.accessories = Accessories.BLANK;
    this.hairColor = HairColor.BROWN;
    this.facialHair = FacialHair.BLANK;
    this.clothes = Clothes.BLAZER_SHIRT;
    this.clothColor = ClothesColor.GRAY1;
    this.eyes = Eyes.WINK;
    this.eyebrow = Eyebrow.DEFAULT_NATURAL;
    this.mouth = Mouth.SMILE;
    this.skin = Skin.LIGHT;
    this.hatColor = HatColor.BLACK;
    this.facialHairColor = FacialHairColor.BLACK;
    this.graphic = Graphic.SKULL;
  }
  ngOnDestroy() {
    this.cleanAvatar();
  }
  cleanAvatar() {
    this.top = null;
    this.face = null;
    this.accessories = null;
    this.hairColor = null;
    this.facialHair = null;
    this.clothes = null;
    this.clothColor = null;
    this.eyes = null;
    this.eyebrow = null;
    this.mouth = null;
    this.skin = null;
    this.hatColor = null;
    this.facialHairColor = null;
    this.graphic = null;
  }
  getRandom() {
    this.style = this.genrateRandom(AvatarStyle);
    this.top = this.genrateRandom(Top);
    this.face = this.genrateRandom(Face);
    this.accessories = this.genrateRandom(Accessories);
    this.hairColor = this.genrateRandom(HairColor);
    this.facialHair = this.genrateRandom(FacialHair);
    this.clothes = this.genrateRandom(Clothes);
    this.clothColor = this.genrateRandom(ClothesColor);
    this.eyes = this.genrateRandom(Eyes);
    this.eyebrow = this.genrateRandom(Eyebrow);
    this.mouth = this.genrateRandom(Mouth);
    this.skin = this.genrateRandom(Skin);
    this.hatColor = this.genrateRandom(HatColor);
    this.facialHairColor = this.genrateRandom(FacialHairColor);
    this.graphic = this.genrateRandom(Graphic);
  }

  private genrateRandom(enu: any) {
    const e = this.getEnumTuple(enu);
    const len = Object.keys(e).length - 1;
    const item = Math.floor(Math.random() * len) + 0;
    return e[item];
  }
  private getEnumTuple(enumRef: any): Array<any> {
    return Object.keys(enumRef).map((key) => {
      return enumRef[key];
    });
  }
}

export enum Accessories {
  BLANK = 'Nada',
  PRESCRIPITON01 = 'Anteojos de cerca',
  PRESCRIPITON02 = 'Anteojos de lejos',
  ROUND = 'Anteojos redondos',
  SUNGLASSES = 'Anteojos de sol',
  WAYFARERS = 'Anteojos Caminante',
  KURTA = 'Kurt Cobain',
}

export enum AvatarStyle {
  CIRCLE = 'circle',
  TRANSPARENT = 'transparent',
}

export enum ClothesColor {
  BLACK = 'Negro',
  BLUE1 = 'Azul 1',
  BLUE2 = 'Azul 2',
  BLUE3 = 'Azul 3',
  GRAY1 = 'Gris 1',
  GRAY2 = 'Gris 2',
  HEATHER = 'Jaspeado',
  PASTEL_BLUE = 'Azul pastel',
  PASTEL_GREEN = 'Verde pastel',
  PASTEL_ORANGE = 'Naranja pastel',
  PASTEL_RED = 'Rojo pastel',
  PASTEL_YELLOW = 'Amarillo pastel',
  PINK = 'Rosa',
  RED = 'Rojo',
  WHITE = 'Blanco',
}

export enum Clothes {
  BLAZER_SHIRT = 'Remera blazer',
  BLAZER_SWEATER = 'Sweater blazer',
  COLLAR_SWEATER = 'Sweater collar',
  GRAPHIC_SHIRT = 'Remera estampada',
  HOODIE = 'Capucha',
  OVERALL = 'Jardinero',
  SHIRT_CREWNECK = 'Remera cuello redondo',
  SHIRT_SCOOPNECK = 'Remera cuello ancho',
  SHIRT_VNECK = 'Remera cuello en V',
}

export enum Eyebrow {
  ANGRY = 'Enojado',
  ANGRY_NATURAL = 'Enojado natural',
  NONE = 'Sin expresión',
  DEFAULT_NATURAL = 'Natural',
  FLAT_NATURAL = 'Fuerte',
  RAISED_EXCITED = 'Exitación',
  RAISED_EXCITED_NATURAL = 'Exitación natural',
  SAD_CONCERNED = 'Preocupado',
  SAD_CONCERNED_NATURAL = 'Preocupado natural',
  UNIBROW_NATURAL = 'Uniceja',
  UPDOWN = 'Arriba y abajo',
  UPDOWN_NATURAL = 'Arriba y abajo natural',
}

export enum Eyes {
  CLOSE = 'Cerrados',
  CRY = 'Llorando',
  NONE = 'Sin expresión',
  DIZZY = 'En X',
  EYEROLL = 'Revoleando los ojos',
  HAPPY = 'Feliz',
  HEARTS = 'Corazón',
  SIDE = 'Mirando al costado',
  SQUINT = 'Entre cerrados',
  SURPRISED = 'Sorprendido',
  WINK = 'Guiñando',
  WINKWACKY = 'Guiñando 2',
}

export enum Face {
  MOUTH = 'mouth',
  NOSE = 'nose',
  EYES = 'eyes',
  EYEBROW = 'eyebrow',
}

export enum FacialHairColor {
  AUBURN = 'Jaspeado',
  BLACK = 'Negro',
  BLONDE = 'Rubio',
  BLONDE_GOLDEN = 'Rubio dorado',
  BROWN = 'Marrón',
  BROWN_DARK = 'Marrón oscuro',
  PASTEL_PINK = 'Rosa pastel',
  PLATINUM = 'Platino',
  RED = 'Rojo',
  SILVER_RED = 'Rojo plata',
}

export enum FacialHair {
  BLANK = 'Sin vello',
  BEARD_LIGHT = 'Barba suave',
  BEARD_MEDIUM = 'Barba mediana',
  BEARD_MAGESTIC = 'Barba majestuosa',
  MOUSTACHE_FANCY = 'Bigote',
  MOUSTACHE_MAGNUM = 'Bigote Magnum',
}

export enum Graphic {
  CUMBIA = 'Cumbia',
  DEER = 'Ciervo',
  DIAMOND = 'Diamond',
  HOLA = 'Hola',
  PIZZA = 'Pizza',
  RESIST = 'Resist',
  SELENA = 'Selena',
  BEAR = 'Oso',
  SKULLOUTLINE = 'Calavera Contorno',
  SKULL = 'Calavera',
}

export enum HairColor {
  AURBURN = 'Pelirrojo',
  BLACK = 'Negro',
  BLONDE = 'Rubio',
  blondegolden = 'Rubio Dorado',
  BROWN = 'Marrón',
  browndark = 'Marrón oscuro',
  pastelpink = 'Rosa pastel',
  PLATINUM = 'Platino',
  RED = 'Rojo',
  silvergray = 'Canoso',
}

export enum HatColor {
  BLACK = 'Negro',
  BLUE01 = 'Azul 1',
  BLUE02 = 'Azul 2',
  BLUE03 = 'Azul 3',
  GRAY01 = 'Gris 1',
  GRAY02 = 'Gris 2',
  HEATHER = 'Jaspeado',
  PASTEL_BLUE = 'Azul pastel',
  PASTEL_GREEN = 'Verde pastel',
  PASTEL_ORANGE = 'Naranja pastel',
  PASTEL_RED = 'Rojo pastel',
  PASTEL_YELLOW = 'Amarillo pastel',
  PINK = 'Rosa',
  RED = 'Rojo',
  WHITE = 'Blanco',
}

export enum Mouth {
  CONCERNED = 'Preocupado',
  NONE = 'Contento',
  DISBELIEF = 'Atónito',
  EATING = 'Comiendo',
  GRIMACE = 'Mueca',
  SAD = 'Triste',
  SCREAM_OPEN = 'Grito',
  SERIOUS = 'Serio',
  SMILE = 'Sonrisa',
  TONGUE = 'Lengua afuera',
  TWINKLE = 'Sonrisa',
}

export enum Skin {
  TANNED = 'Bronceado',
  YELLOW = 'Simpson',
  PALE = 'Pálido',
  LIGHT = 'Clara',
  BROWN = 'Marrón',
  DARK_BROWN = 'Marrón oscuro',
  BLACK = 'Oscura',
}

export enum Top {
  NO_HAIR = 'Pelado',
  WINTER_HAT1 = 'Gorro de invierno 1',
  WINTER_HAT2 = 'Gorro de invierno 2',
  WINTER_HAT3 = 'Gorro de invierno 3',
  WINTER_HAT4 = 'Gorro de invierno 4',
  LONGHAIR_BIGHAIR = 'Cabello largo porra',
  LONGHAIR_BOB = 'Cabello largo carré',
  LONGHAIR_BUN = 'Cabello largo y rodete',
  LONGHAIR_CURLY = 'Cabello largo rulos',
  LONGHAIR_CURVY = 'Cabello largo con ondas',
  LONGHAIR_DREADS = 'Cabello largo con rastas',
  LONGHAIR_FRIDA = 'Cabello largo Frida',
  LONGHAIR_FRO = 'Afro',
  LONGHAIR_FROBAND = 'Super Afro',
  LONGHAIR_NOTTOOLONG = 'Cabello largo',
  LONGHAIRS_HAVEDSIDES = 'Largo y rapado a los costados',
  LONGHAIR_MIAWALLACE = 'Mia Wallace',
  LONGHAIR_STRAIGHT = 'Cabello largo liso',
  LONGHAIR_STRAIGHT2 = 'Cabello largo liso 2',
  LONGHAIR_STRAIGHTSTRAND = 'Cabello largo liso 3',
  SHORTHAIR_DREADS01 = 'Rastas cortas',
  SHORTHAIR_DREADS02 = 'Rastas cortas 2',
  SHORTHAIR_FRIZZLE = 'Jopo',
  SHORTHAIR_SHORTFLAT = 'Cabello corto',
  SHORTHAIR_SHAGGYMULLET = 'Cabello corto desprolijo',
  SHORTHAIR_SHORTCURLY = 'Cabello corto con rulos',
  SHORTHAIR_SHORTWAVED = 'Cabello corto con jopo',
  SHORTHAIR_SIDES = 'Pelo a los costados',
  SHORTHAIR_THECAESAR = 'Julio César',
  SHORTHAIR_THECAESARSIDEPART = 'Julio César 2',
  SHORTHAIR_SHORTROUND = 'Casi rapado',
  EYEPATCH = 'Parche en el ojo',
  HAT = 'Gorro',
  HIJAB = 'Hijab',
  TURBAN = 'Turbante',
}
