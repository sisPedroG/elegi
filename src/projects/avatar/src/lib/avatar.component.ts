import { Component, OnInit, Input } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { AvatarHelper } from './parts/avatar';
import { AvatarOptions } from './avatar.enum';

@Component({
  selector: 'avatar',
  template: `<div id="svgid" [innerHtml]="getSvg()"></div> `,
  styles: [],
})
export class AvatarComponent implements OnInit {
  @Input() options: AvatarOptions;
  @Input() width: string;
  @Input() height: string;

  constructor(private sanitizer: DomSanitizer) {}

  ngOnInit() {}

  getSvg() {
    if (this.width == undefined || this.width == undefined) {
      this.width = '264px';
      this.height = '280px';
    }
    return this.sanitizer.bypassSecurityTrustHtml(
      AvatarHelper.getSvg(this.options, this.width, this.height)
    );
  }
  ngOnDestroy() {}
}
