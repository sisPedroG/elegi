import { Injectable } from '@angular/core';
import { AppService } from 'src/app/providers/app/app.service';
import { UserDataService } from 'src/app/providers/user-data.service';
import { AvatarOptions } from '../../../../projects/avatar/src/public-api';

@Injectable({
  providedIn: 'root',
})
export class AvatarService {
  avatar: AvatarOptions;
  mostrarAvatar: boolean = false;

  constructor(public service: AppService, public userData: UserDataService) {
    this.userData.getAvatar().then(async (data) => {
      if (data == null || data == undefined) {
        this.avatar = new AvatarOptions();
      } else {
        this.avatar = data;
      }
      // setTimeout(() => {
      //   return this.toggleSvg(false);
      // }, 0);
      // this.showColourFabric();
      this.mostrarAvatar = true;
    });
  } //TODO  almacenar aca los datos del avatar para

  actualizarAvatar(avatarOptions: AvatarOptions) {
    this.avatar = avatarOptions;
  }
  async setAvatar() {
    this.avatar = await this.userData.getAvatar();
  }
}
