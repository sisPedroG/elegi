import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

import {
  AvatarOptions,
  AvatarService,
} from '../../projects/avatar/src/public-api';
import {
  Clothes,
  FacialHair,
  Top,
  Eyes,
  Mouth,
  Skin,
  Accessories,
  Eyebrow,
  Face,
  Graphic,
  FacialHairColor,
  ClothesColor,
  HatColor,
  HairColor,
  AvatarStyle,
} from '../../projects/avatar/src/public-api';
import { Router } from '@angular/router';
import { AppService } from '../providers/app/app.service';
import { UserDataService } from '../providers/user-data.service';
import { AlertController } from '@ionic/angular';
@Component({
  selector: 'app-avatar',
  templateUrl: './avatar.page.html',
  styleUrls: ['./avatar.page.scss'],
})
export class AvatarPage implements OnInit {
  constructor(
    private router: Router,
    public service: AppService,
    public userData: UserDataService,
    public avatarService: AvatarService,
    public alertCtrl: AlertController
  ) {}
  hayCambios: boolean = false;
  topsEnum = Top;
  facialHairEnum = FacialHair;
  clothesEnum = Clothes;

  accessories: Array<any>;
  clothColor: Array<any>;
  clothe: Array<any>;
  eyebrow: Array<any>;
  eyes: Array<any>;
  face: Array<any>;
  facialHair: Array<any>;
  facialHairColor: Array<any>;
  graphic: Array<any>;
  hairColor: Array<any>;
  hatColor: Array<any>;
  mouth: Array<any>;
  skin: Array<any>;
  tops: Array<any>;
  avatarStyle: Array<any>;
  avatarForm: FormGroup;
  public options: AvatarOptions;
  showAngular = false;
  showImage = false;
  showSvg = false;
  svgData: string;
  paginaPrev: any;
  async ngOnInit() {
    if (this.router.getCurrentNavigation().extras.state != undefined) {
      this.paginaPrev = this.router.getCurrentNavigation().extras.state.paginaPrev;
    }
    this.options = new AvatarOptions();

    // this.paginaPrev = this.router.getCurrentNavigation().extras.state || {};
    this.avatarForm = new FormGroup({
      avatarStyle: new FormControl(this.options.style),
      top: new FormControl(this.options.top),
      accessories: new FormControl(this.options.accessories),
      hairColor: new FormControl(this.options.hairColor),
      hatColor: new FormControl(this.options.hatColor),
      facialHair: new FormControl(this.options.facialHair),
      facialHairColor: new FormControl(this.options.facialHairColor),
      clothes: new FormControl(this.options.clothes),
      clothColor: new FormControl(this.options.clothColor),
      eyes: new FormControl(this.options.eyes),
      eyebrow: new FormControl(this.options.eyebrow),
      mouth: new FormControl(this.options.mouth),
      skin: new FormControl(this.options.skin),
      face: new FormControl(this.options.face),
      graphic: new FormControl(this.options.graphic),
    });
    this.tops = this.getEnumTupple(Top);
    this.facialHair = this.getEnumTupple(FacialHair);
    this.clothe = this.getEnumTupple(Clothes);
    this.eyes = this.getEnumTupple(Eyes);
    this.eyebrow = this.getEnumTupple(Eyebrow);
    this.mouth = this.getEnumTupple(Mouth);
    this.skin = this.getEnumTupple(Skin);
    this.accessories = this.getEnumTupple(Accessories);
    this.clothColor = this.getEnumTupple(ClothesColor);
    this.face = this.getEnumTupple(Face);
    this.facialHairColor = this.getEnumTupple(FacialHairColor);
    this.graphic = this.getEnumTupple(Graphic);
    this.hatColor = this.getEnumTupple(HatColor);
    this.hairColor = this.getEnumTupple(HairColor);
    this.avatarStyle = this.getEnumTupple(AvatarStyle);
    // this.avatarForm.patchValue({
    //   top: this.avatarService.avatar.top,
    //   accessories: this.avatarService.avatar.accessories,
    //   hairColor: this.avatarService.avatar.hairColor,
    //   hatColor: this.avatarService.avatar.hatColor,
    //   facialHair: this.avatarService.avatar.facialHair,
    //   facialHairColor: this.avatarService.avatar.facialHairColor,
    //   clothes: this.avatarService.avatar.clothes,
    //   clothColor: this.avatarService.avatar.clothColor,
    //   eyes: this.avatarService.avatar.eyes,
    //   eyebrow: this.avatarService.avatar.eyebrow,
    //   mouth: this.avatarService.avatar.mouth,
    //   skin: this.avatarService.avatar.skin,
    //   face: this.avatarService.avatar.face,
    //   graphic: this.avatarService.avatar.graphic,
    // });
    this.avatarForm.valueChanges.subscribe((value) => {
      this.hayCambios = true;
      this.options = value;
      this.avatarService.actualizarAvatar(value);
      setTimeout(() => {
        return this.toggleSvg(false);
      }, 0);
      // this.router.navigate([], {
      //   queryParams: {
      //     avatarStyle: this.options.style,
      //     top: this.options.top,
      //     accessories: this.options.accessories,
      //     hairColor: this.options.hairColor,
      //     hatColor: this.options.hatColor,
      //     facialHair: this.options.facialHair,
      //     facialHairColor: this.options.facialHairColor,
      //     clothes: this.options.clothes,
      //     clothColor: this.options.clothColor,
      //     eyes: this.options.eyes,
      //     eyebrow: this.options.eyebrow,
      //     mouth: this.options.mouth,
      //     skin: this.options.skin,
      //     face: this.options.face,
      //     graphic: this.options.graphic,
      //   },
      // });
      // this.showColourFabric();
    });
  }
  getEnumTupple(enumRef: any): Array<any> {
    return Object.keys(enumRef).map((key) => {
      return enumRef[key];
    });
  }
  showColourFabric() {
    if (
      this.avatarService.avatar.clothes === this.clothesEnum.BLAZER_SHIRT ||
      this.avatarService.avatar.clothes === this.clothesEnum.BLAZER_SWEATER
    ) {
      return false;
    }
    return true;
  }
  showHatColour() {
    if (
      this.avatarService.avatar.top === this.topsEnum.HIJAB ||
      this.avatarService.avatar.top === this.topsEnum.TURBAN ||
      this.avatarService.avatar.top === this.topsEnum.WINTER_HAT1 ||
      this.avatarService.avatar.top === this.topsEnum.WINTER_HAT2 ||
      this.avatarService.avatar.top === this.topsEnum.WINTER_HAT3 ||
      this.avatarService.avatar.top === this.topsEnum.WINTER_HAT4
    ) {
      return true;
    } else return false;
  }

  genrateRandom(enu: any) {
    const e = this.getEnumTupple(enu);
    const len = Object.keys(e).length - 1;
    const item = Math.floor(Math.random() * len) + 0;
    return e[item];
  }

  showHairColour() {
    if (
      this.avatarService.avatar.top === this.topsEnum.LONGHAIR_BIGHAIR ||
      this.avatarService.avatar.top === this.topsEnum.LONGHAIR_BOB ||
      this.avatarService.avatar.top === this.topsEnum.LONGHAIR_BUN ||
      this.avatarService.avatar.top === this.topsEnum.LONGHAIR_CURLY ||
      this.avatarService.avatar.top === this.topsEnum.LONGHAIR_CURVY ||
      this.avatarService.avatar.top === this.topsEnum.LONGHAIR_DREADS ||
      this.avatarService.avatar.top === this.topsEnum.LONGHAIR_FRO ||
      this.avatarService.avatar.top === this.topsEnum.LONGHAIR_FROBAND ||
      this.avatarService.avatar.top === this.topsEnum.LONGHAIR_NOTTOOLONG ||
      this.avatarService.avatar.top === this.topsEnum.LONGHAIR_MIAWALLACE ||
      this.avatarService.avatar.top === this.topsEnum.LONGHAIR_STRAIGHT ||
      this.avatarService.avatar.top === this.topsEnum.LONGHAIR_STRAIGHT2 ||
      this.avatarService.avatar.top === this.topsEnum.LONGHAIR_STRAIGHTSTRAND ||
      this.avatarService.avatar.top === this.topsEnum.SHORTHAIR_DREADS01 ||
      this.avatarService.avatar.top === this.topsEnum.SHORTHAIR_DREADS02 ||
      this.avatarService.avatar.top === this.topsEnum.SHORTHAIR_FRIZZLE ||
      this.avatarService.avatar.top === this.topsEnum.SHORTHAIR_SHAGGYMULLET ||
      this.avatarService.avatar.top === this.topsEnum.SHORTHAIR_SHORTCURLY ||
      this.avatarService.avatar.top === this.topsEnum.SHORTHAIR_SHORTFLAT ||
      this.avatarService.avatar.top === this.topsEnum.SHORTHAIR_SHORTROUND ||
      this.avatarService.avatar.top === this.topsEnum.SHORTHAIR_SHORTWAVED ||
      this.avatarService.avatar.top === this.topsEnum.SHORTHAIR_SIDES ||
      this.avatarService.avatar.top === this.topsEnum.SHORTHAIR_THECAESAR ||
      this.avatarService.avatar.top ===
        this.topsEnum.SHORTHAIR_THECAESARSIDEPART
    ) {
      return true;
    } else return false;
  }
  ionViewWillEnter() {}
  async presentAlert() {
    const alert = await this.alertCtrl.create({
      header: 'Avatar',
      message: '¿Quieres finalizar la creación de tu avatar?',
      buttons: [
        {
          text: 'Cancelar',
          cssClass: 'secondary',
          handler: (blah) => {},
        },
        {
          text: 'Aceptar',
          cssClass: 'primary',
          handler: () => {
            this.grabarAvatar();
          },
        },
      ],
    });

    await alert.present();
  }
  grabarAvatar() {
    if (this.hayCambios) {
      this.userData.setAvatar(this.avatarForm.value).then((data) => {
        this.avatarService.actualizarAvatar(this.avatarForm.value);
        const svgNode = document.getElementById('svgid');
        svgNode.innerHTML = '';
      });
      this.service.enviarAvatar(this.avatarForm.value).then((data) => {});
    }
    const svgNode = document.getElementById('svgid');
    svgNode.innerHTML = '';
    if (this.paginaPrev == 'perfil') {
      this.router.navigate(['/tabs/perfil']);
    } else {
      this.router.navigate(['/tabs/novedades']);
    }
  }

  getRandom() {
    this.options = new AvatarOptions();

    this.options.getRandom();
    this.avatarForm.patchValue({
      // avatarStyle: AvatarStyle.CIRCLE,
      top: this.options.top,
      accessories: this.options.accessories,
      hairColor: this.options.hairColor,
      hatColor: this.options.hatColor,
      facialHair: this.options.facialHair,
      facialHairColor: this.options.facialHairColor,
      clothes: this.options.clothes,
      clothColor: this.options.clothColor,
      eyes: this.options.eyes,
      eyebrow: this.options.eyebrow,
      mouth: this.options.mouth,
      skin: this.options.skin,
      face: this.options.face,
      graphic: this.options.graphic,
    });
    this.avatarForm.patchValue({
      // avatarStyle: AvatarStyle.CIRCLE,
      top: this.avatarService.avatar.top,
      accessories: this.avatarService.avatar.accessories,
      hairColor: this.avatarService.avatar.hairColor,
      hatColor: this.avatarService.avatar.hatColor,
      facialHair: this.avatarService.avatar.facialHair,
      facialHairColor: this.avatarService.avatar.facialHairColor,
      clothes: this.avatarService.avatar.clothes,
      clothColor: this.avatarService.avatar.clothColor,
      eyes: this.avatarService.avatar.eyes,
      eyebrow: this.avatarService.avatar.eyebrow,
      mouth: this.avatarService.avatar.mouth,
      skin: this.avatarService.avatar.skin,
      face: this.avatarService.avatar.face,
      graphic: this.avatarService.avatar.graphic,
    });
  }
  async toggleSvg(bool) {
    if (bool) {
      this.showSvg = !this.showSvg;
      this.showAngular = false;
      this.showImage = false;
    }

    const svgNode = document.getElementById('svgid');
    this.svgData = svgNode.innerHTML;
  }
}
