import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { OneSignal } from '@ionic-native/onesignal/ngx';
import { MenuController, Platform } from '@ionic/angular';
import { AppService } from './providers/app/app.service';
import { UserDataService } from './providers/user-data.service';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  constructor(
    public userData: UserDataService,
    public router: Router,
    public menu: MenuController,
    public platform: Platform,
    public oneSignal: OneSignal,
    public service: AppService,
    public screenOrientation: ScreenOrientation
  ) {
    this.initializeApp();
  }
  initializeApp() {
    this.platform.ready().then(() => {
      this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);

      this.oneSignal
        .startInit('d2f11c08-0101-4b2a-867f-a470282bd09e', '627260169777')
        .endInit();
      //.handleNotificationReceived(this.notificationOpenedCallback);
      this.oneSignal.getIds().then((data) => {
        console.log(data);
        this.service.enviarPlayerId(data.userId).then((data) => {
          console.log(data);
        });
        this.userData.setIdOnesignal(data.userId).then((data) => {
          console.log(data);
        });
      });
      this.oneSignal.sendTag('versionApp', this.service.versionApp);
      this.oneSignal.inFocusDisplaying(
        this.oneSignal.OSInFocusDisplayOption.InAppAlert
      );

      this.oneSignal.handleNotificationOpened().subscribe((data) => {
        // do something when a notification is opened
        if (data.notification.payload.additionalData['pagina'] !== undefined) {
          this.navigateByPush(data.notification.payload.additionalData.pagina);
          // this.router.navigate(
          //   data.notification.payload.additionalData['pagina']
          // );
        }
      });

      this.oneSignal.endInit();
    });

    this.userData.isLoggedIn().then((res) => {
      res
        ? this.router.navigate(['/tutorial'])
        : this.router.navigate(['/login']);
    });
    this.menu.enable(true);
  }
  notificationOpenedCallback(jsonData) {

    if (jsonData.payload.additionalData.pagina !== undefined) {
      this.navigateByPush(jsonData.payload.additionalData.pagina);
    }
  }
  navigateByPush(page) {
    console.log(page);
    this.router
      .navigate([page])
      .then((data) => {
        console.log(data);
        console.log('redirecciono');
      })
      .catch((e) => {
        console.log('hubo un error');
        console.log(e);
      });
  }
  openSideMenu() {
    this.menu.open('end');
  }
  checkLogin() {
    return;
  }
}
