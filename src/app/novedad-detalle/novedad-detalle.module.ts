import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NovedadDetallePageRoutingModule } from './novedad-detalle-routing.module';

import { NovedadDetallePage } from './novedad-detalle.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NovedadDetallePageRoutingModule
  ],
  declarations: [NovedadDetallePage]
})
export class NovedadDetallePageModule {}
