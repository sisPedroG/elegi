import { Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { novedadInt } from '../utils/interfaces';
@Component({
  selector: 'app-novedad-detalle',
  templateUrl: './novedad-detalle.page.html',
  styleUrls: ['./novedad-detalle.page.scss'],
})
export class NovedadDetallePage implements OnInit {
  safeURLForm: SafeResourceUrl;
  safeURLCuerpo: SafeResourceUrl | String = '';
  mostrarFormulario: boolean = false;
  mostrarVideo: boolean = false;
  mostrarPuntos: boolean = false;
  novedad: any;
  constructor(private router: Router, private _sanitizer: DomSanitizer) {}
  ngOnInit() {
    // this.novedad = {
    //   idArticulo: 3,
    //   titulo: 'PAE Respira',
    //   texto: 'Talleres de respiración y meditación para tu bienestar.',
    //   destacado: '1',
    //   imagen:
    //     'https://elgourmet.s3.amazonaws.com/recetas/cover/tarta_qZEDFgPHQsGelh37irYd1njc0M9BWy.png',
    //   link: '2',
    //   categoria: 'Arte y Cultura',
    //   likes: 0,
    //   fechaDeCreacion: '2021-02-25',
    //   comoFormarParte: null,
    //   // fechaEvento: null,
    // };
    this.novedad = this.router.getCurrentNavigation().extras.state;
    // this.novedad.cuerpo
    let tipoCuerpo = '';
    switch (this.novedad.tipoArticulo) {
      case 'evento':
        tipoCuerpo = 'cuerpoEvento';
        break;
      case 'programa':
        tipoCuerpo = 'cuerpoPrograma';
        break;
      case 'novedad':
        tipoCuerpo = 'cuerpoNovedad';
        break;

      default:
        break;
    }
    this.safeURLCuerpo = this._sanitizer.bypassSecurityTrustHtml(
      this.novedad[tipoCuerpo]
    );
    if (this.novedad.formulario != undefined) {
      this.mostrarFormulario = true;
      this.safeURLForm = this._sanitizer.bypassSecurityTrustResourceUrl(
        this.novedad.formulario
      );
    }
    if (this.novedad.puntos != undefined) this.mostrarPuntos = true;
  }
  volver() {
    this.router.navigate(['/tabs/novedades']);
  }
  ionViewDidLeave() {
    console.log('did liv');
    this.novedad = {};
  }
}
