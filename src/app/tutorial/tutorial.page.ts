import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { IonSlides, MenuController } from '@ionic/angular';
import { UserDataService } from '../providers/user-data.service';

@Component({
  selector: 'app-tutorial',
  templateUrl: './tutorial.page.html',
  styleUrls: ['./tutorial.page.scss'],
})
export class TutorialPage implements OnInit {
  @ViewChild('slides', { static: true }) slides: IonSlides;
  showSkip = true;

  constructor(
    public menu: MenuController,
    public router: Router,
    public userData: UserDataService
  ) {}

  ngOnInit() {}
  async startApp() {
    this.router
      .navigate(['/cambiarnickname'], { replaceUrl: true })
      .then(() => this.userData.setTutorialVisto());
  }

  onSlideChangeStart(event) {
    event.target.isEnd().then((isEnd) => {
      this.showSkip = !isEnd;
    });
  }
  async goNext() {
    if (await this.slides.isEnd()) {
      this.startApp();
    } else {
      this.slides.slideNext();
    }
  }
  ionViewWillEnter() {
    this.userData.vioTutorial().then((res) => {
      if (res === true) {
        this.router.navigate(['/tabs/novedades'], { replaceUrl: true });
      }
    });
    this.menu.enable(false);
  }

  ionViewDidLeave() {
    // enable the root left menu when leaving the tutorial page
    this.menu.enable(true);
  }
}
