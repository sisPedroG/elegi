import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppService } from '../providers/app/app.service';

@Component({
  selector: 'app-encuestas',
  templateUrl: './encuestas.page.html',
  styleUrls: ['./encuestas.page.scss'],
})
export class EncuestasPage implements OnInit {
  encuestas: Array<any>;
  hayEncuestas: boolean = false;
  constructor(
    public router: Router,
    public location: Location,
    public service: AppService
  ) {}

  ngOnInit() {
    this.service.obtenerEncuestas().then((data) => {
      console.log(data);
      this.encuestas = data['encuestas'];
      if (this.encuestas != null) {
        this.hayEncuestas = true;
      }
      console.log(this.encuestas);
    });
  }
  navigate(e) {
    this.router.navigate(['encuesta-detalle'], { state: e });
  }
  volver() {
    this.location.back();
  }
}
