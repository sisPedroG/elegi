import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { ToastController } from '@ionic/angular';
import { AppService } from '../providers/app/app.service';
import { NivelesService } from '../providers/niveles.service';
import { puntos } from '../utils/interfaces';

@Component({
  selector: 'app-escaner-qr',
  templateUrl: './escaner-qr.page.html',
  styleUrls: ['./escaner-qr.page.scss'],
})
export class EscanerQrPage implements OnInit {
  data: any;
  codigo: string = '';
  ocultarModal: boolean = true;
  puntos: puntos = {};
  modalText: string;
  constructor(
    public barcodeScanner: BarcodeScanner,
    public location: Location,
    public service: AppService,
    public toastController: ToastController,
    public nivelesService: NivelesService
  ) {}

  ngOnInit() {
    // this.startScan();
  }
  volver() {
    this.location.back();
  }
  async presentToast(msg) {
    let color;
    // exito == 1 ? (color = 'success') : ();
    color = 'warning';
    const toast = await this.toastController.create({
      message: msg,
      duration: 3500,
      cssClass: 'toastCustomWarning',
      // color: color,
    });
    toast.present();
  }
  enviarCodigo() {
    if (this.codigo == '') {
    } else {
      this.service.enviarCodigo(this.codigo).then((data) => {
        this.codigo = '';
        console.log(data);
        // this.presentToast(data['mensaje'], data['exito']);
        if (data['exito'] == 1) {
          this.ocultarModal = false;
          this.modalText = data['mensaje'];
          this.nivelesService.obtenerAtributos();
          Object.keys(data['puntos']).forEach((atributo) => {
            this.puntos[atributo] = data['puntos'][atributo];
          });
          console.log(this.puntos);
        } else {
          this.presentToast(data['mensaje']);
        }
      });
    }
  }
  ocultarModalFn() {
    console.log('ocultar');
    this.ocultarModal = true;
    this.puntos = {};
  }
  scan() {
    this.data = null;
    this.barcodeScanner
      .scan()
      .then((barcodeData) => {
        this.codigo = barcodeData.text;
        // this.data = barcodeData;
        this.enviarCodigo();
      })
      .catch((err) => {
        console.log('Error', err);
      });
  }
  // async checkPermission() {
  //   // check or request permission
  //   const status = await BarcodeScanner.checkPermission({ force: true });

  //   if (status.granted) {
  //     // the user granted permission
  //     return true;
  //   }

  //   return false;
  // }
  // async startScan() {
  //   this.qrScanner
  //     .prepare()
  //     .then((status: QRScannerStatus) => {
  //       if (status.authorized) {
  //         // camera permission was granted

  //         // start scanning
  //         let scanSub = this.qrScanner.scan().subscribe((text: string) => {

  //           this.qrScanner.hide(); // hide camera preview
  //           scanSub.unsubscribe(); // stop scanning
  //         });
  //       } else if (status.denied) {
  //         // camera permission was permanently denied
  //         // you must use QRScanner.openSettings() method to guide the user to the settings page
  //         // then they can grant the permission from there
  //       } else {
  //         // permission was denied, but not permanently. You can ask for permission again at a later time.
  //       }
  //     })
  //     .catch((e: any) => console.log('Error is', e));
  // }
}
