import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EscanerQrPageRoutingModule } from './escaner-qr-routing.module';

import { EscanerQrPage } from './escaner-qr.page';
import { ComponentsModule } from '../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EscanerQrPageRoutingModule,
    ComponentsModule,
  ],
  declarations: [EscanerQrPage],
})
export class EscanerQrPageModule {}
