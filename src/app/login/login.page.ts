import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController, ToastController } from '@ionic/angular';
import { AppService } from '../providers/app/app.service';
import { UserDataService } from 'src/app/providers/user-data.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  // @ViewChild('nroTramite') myInput; //para setear input focus en el campo dni
  esconderBoton: boolean = false;
  numeroDNI: number;
  loading: any;
  constructor(
    private loadingCtrl: LoadingController,
    private service: AppService,
    private router: Router,
    public userData: UserDataService,
    public toastController: ToastController
  ) {}

  ngOnInit() {
    this.service.loginInvitado().then((data) => {
      if (data == 1) {
        this.esconderBoton = true;
      }
    });
  }

  gotoNextField(event?) {
    if (event.key === 'Enter') {
      // this.myInput.setFocus();
    }
    // if (event.key == "Unidentified") {
    //   let tempDNI=String(this.dni);
    //   this.dni = tempDNI.slice(0,-1)

    // }
  }

  ingresarComoInvitado() {
    this.registrarUsuario({
      idUsuario: '1',
      token:
        'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJJZCI6IjEiLCJGZWNoYUhvcmEiOiIyMDIxLTA0LTAxIDE5OjU0OjUwIn0.zAMhoq4PVnjBRoOjw4dRwenNRmhoXavCrO2aSTnrUxA',
      estado: 'A',
      fcmNombreUG: 'buenosAires',
      nickname: 'Invitado',
    });
    this.userData.vioTutorial().then((res) => {
      if (res === true) {
        this.router.navigate(['/tabs/novedades'], { replaceUrl: true });
      } else {
        this.router.navigate(['/tutorial']);
      }
      this.userData.login();
    });
  }
  async presentToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 1000,
      color: 'danger',
    });
    toast.present();
  }
  verificarUsuario() {
    if (this.numeroDNI != null) {
      this.service.login(this.numeroDNI).then((data: any) => {
        if (data.message == 'ok') {
          this.registrarUsuario(data.user);
          this.userData.vioTutorial().then((res) => {
            if (res === true) {
              this.router.navigate(['/tabs/novedades'], { replaceUrl: true });
            } else {
              this.router.navigate(['/tutorial'], { replaceUrl: true });
            }
          });
        } else {
          this.presentToast('DNI inválido o incorrecto.');
        }
      });
    }
  }
  registrarUsuario(data: any) {
    this.userData.setToken(data.token);
    this.userData.setNickname(data.nickname);
    this.userData.setNumeroDNI(this.numeroDNI);
    this.userData.setFcmNombreUG(data.fcmNombreUG);
    this.userData.setIdUsuario(data.idUsuario);
    this.userData.nombreKeys();
    this.userData.login();
  }
}
