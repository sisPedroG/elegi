import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LogrosDetallePageRoutingModule } from './logros-detalle-routing.module';

import { LogrosDetallePage } from './logros-detalle.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LogrosDetallePageRoutingModule
  ],
  declarations: [LogrosDetallePage]
})
export class LogrosDetallePageModule {}
