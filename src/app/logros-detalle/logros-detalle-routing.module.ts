import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LogrosDetallePage } from './logros-detalle.page';

const routes: Routes = [
  {
    path: '',
    component: LogrosDetallePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LogrosDetallePageRoutingModule {}
