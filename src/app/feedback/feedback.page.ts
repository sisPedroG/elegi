import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { AlertController, ToastController } from '@ionic/angular';
import { AppService } from '../providers/app/app.service';

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.page.html',
  styleUrls: ['./feedback.page.scss'],
})
export class FeedbackPage implements OnInit {
  feedback: string = '';
  constructor(
    public location: Location,
    public service: AppService,
    public toastController: ToastController,
    public alertCtrl: AlertController
  ) {}

  ngOnInit() {}

  volver() {
    this.location.back();
  }

  enviarFeedback() {
    if (this.feedback != '') {
      this.service.enviarFeedback(this.feedback).then((data) => {
        if ((data = 'ok')) {
          this.presentToast('Gracias por tu comentario!');
          this.feedback = '';
        } else {
          this.presentToast(
            'Hubo un problema al ingresar su comentario. Intente nuevamente'
          );
        }
      });
    }
  }
  async presentToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000,
      color: 'success',
    });
    toast.present();
  }
  async presentAlert() {
    const alert = await this.alertCtrl.create({
      header: 'Atención',
      message: 'Enviar feedback?',
      buttons: [
        {
          text: 'Cancelar',
          cssClass: 'secondary',
          handler: (blah) => {
          },
        },
        {
          text: 'Aceptar',
          cssClass: 'primary',
          handler: () => {
            this.enviarFeedback();
          },
        },
      ],
    });

    await alert.present();
  }
}
