import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { CheckTutorial } from './providers/viotutorial.service';
const routes: Routes = [
  {
    path: '',
    loadChildren: () =>
      import('./tabs/tabs.module').then((m) => m.TabsPageModule),
  },
  {
    path: 'perfil',
    loadChildren: () =>
      import('./perfil/perfil.module').then((m) => m.PerfilPageModule),
  },
  {
    path: 'clases',
    loadChildren: () =>
      import('./clases/clases.module').then((m) => m.ClasesPageModule),
  },
  {
    path: 'roadtrips',
    loadChildren: () =>
      import('./roadtrips/roadtrips.module').then((m) => m.RoadtripsPageModule),
  },
  {
    path: 'clasesDetalle/:id',
    loadChildren: () =>
      import('./clase-detalle/clase-detalle.module').then(
        (m) => m.ClaseDetallePageModule
      ),
  },
  {
    path: 'roadtripDetalle/:id',
    loadChildren: () =>
      import('./roadtrip-detalle/roadtrip-detalle.module').then(
        (m) => m.RoadtripDetallePageModule
      ),
  },
  {
    path: 'roadtrip-video',
    loadChildren: () =>
      import('./roadtrip-video/roadtrip-video.module').then(
        (m) => m.RoadtripVideoPageModule
      ),
  },

  {
    path: 'login',
    loadChildren: () =>
      import('./login/login.module').then((m) => m.LoginPageModule),
  },
  {
    path: 'novedadDetalle',
    loadChildren: () =>
      import('./novedad-detalle/novedad-detalle.module').then(
        (m) => m.NovedadDetallePageModule
      ),
  },
  {
    path: 'tutorial',
    loadChildren: () =>
      import('./tutorial/tutorial.module').then((m) => m.TutorialPageModule),
    canLoad: [CheckTutorial],
  },
  {
    path: 'escanerQR',
    loadChildren: () =>
      import('./escaner-qr/escaner-qr.module').then(
        (m) => m.EscanerQrPageModule
      ),
  },
  {
    path: 'trivia',
    loadChildren: () =>
      import('./trivia/trivia.module').then((m) => m.TriviaPageModule),
  },
  {
    path: 'avatar',
    loadChildren: () =>
      import('./avatar/avatar.module').then((m) => m.AvatarPageModule),
  },
  {
    path: 'mas',
    loadChildren: () => import('./mas/mas.module').then((m) => m.MasPageModule),
  },
  {
    path: 'faq',
    loadChildren: () => import('./faq/faq.module').then((m) => m.FaqPageModule),
  },
  {
    path: 'encuestas',
    loadChildren: () =>
      import('./encuestas/encuestas.module').then((m) => m.EncuestasPageModule),
  },
  {
    path: 'encuesta-detalle',
    loadChildren: () =>
      import('./encuesta-detalle/encuesta-detalle.module').then(
        (m) => m.EncuestaDetallePageModule
      ),
  },
  {
    path: 'logros-detalle',
    loadChildren: () =>
      import('./logros-detalle/logros-detalle.module').then(
        (m) => m.LogrosDetallePageModule
      ),
  },
  {
    path: 'feedback',
    loadChildren: () =>
      import('./feedback/feedback.module').then((m) => m.FeedbackPageModule),
  },
  {
    path: 'cambiarnickname',
    loadChildren: () => import('./cambiarnickname/cambiarnickname.module').then( m => m.CambiarnicknamePageModule)
  },
  {
    path: 'participaciones-eventos',
    loadChildren: () => import('./participaciones-eventos/participaciones-eventos.module').then( m => m.ParticipacionesEventosPageModule)
  },
  {
    path: 'participaciones-clases',
    loadChildren: () => import('./participaciones-clases/participaciones-clases.module').then( m => m.ParticipacionesClasesPageModule)
  },
  {
    path: 'mis-misiones',
    loadChildren: () => import('./mis-misiones/mis-misiones.module').then( m => m.MisMisionesPageModule)
  },
  {
    path: 'ranking',
    loadChildren: () => import('./ranking/ranking.module').then( m => m.RankingPageModule)
  },
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      preloadingStrategy: PreloadAllModules,
      // enableTracing: true,
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
