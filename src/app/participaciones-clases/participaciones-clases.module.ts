import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ParticipacionesClasesPageRoutingModule } from './participaciones-clases-routing.module';

import { ParticipacionesClasesPage } from './participaciones-clases.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ParticipacionesClasesPageRoutingModule
  ],
  declarations: [ParticipacionesClasesPage]
})
export class ParticipacionesClasesPageModule {}
