import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ParticipacionesClasesPage } from './participaciones-clases.page';

const routes: Routes = [
  {
    path: '',
    component: ParticipacionesClasesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ParticipacionesClasesPageRoutingModule {}
