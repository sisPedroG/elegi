import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { NivelesService } from '../providers/niveles.service';

@Component({
  selector: 'app-participaciones-clases',
  templateUrl: './participaciones-clases.page.html',
  styleUrls: ['./participaciones-clases.page.scss'],
})
export class ParticipacionesClasesPage implements OnInit {
  constructor(public location: Location, public nivelesS: NivelesService) {}

  ngOnInit() {}
  volver() {
    this.location.back();
  }
}
