import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ParticipacionesEventosPageRoutingModule } from './participaciones-eventos-routing.module';

import { ParticipacionesEventosPage } from './participaciones-eventos.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ParticipacionesEventosPageRoutingModule
  ],
  declarations: [ParticipacionesEventosPage]
})
export class ParticipacionesEventosPageModule {}
