import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { NivelesService } from '../providers/niveles.service';

@Component({
  selector: 'app-participaciones-eventos',
  templateUrl: './participaciones-eventos.page.html',
  styleUrls: ['./participaciones-eventos.page.scss'],
})
export class ParticipacionesEventosPage implements OnInit {
  constructor(public location: Location, public nivelesS: NivelesService) {}

  ngOnInit() {}
  volver() {
    this.location.back();
  }
}
