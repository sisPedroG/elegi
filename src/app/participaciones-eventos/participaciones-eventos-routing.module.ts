import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ParticipacionesEventosPage } from './participaciones-eventos.page';

const routes: Routes = [
  {
    path: '',
    component: ParticipacionesEventosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ParticipacionesEventosPageRoutingModule {}
