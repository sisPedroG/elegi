import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { ClasesService } from '../providers/clases.service';
import { UserDataService } from '../providers/user-data.service';
import { clase } from '../utils/interfaces';

@Component({
  selector: 'app-clases',
  templateUrl: './clases.page.html',
  styleUrls: ['./clases.page.scss'],
})
export class ClasesPage implements OnInit {
  segment: string;
  clases: Array<clase> = [];
  listaClases: Array<clase> = [];
  hayClasesEnVivo: boolean = true;
  datosCargados = false;
  constructor(
    public router: Router,
    public userData: UserDataService,
    public loadingController: LoadingController,
    public claseService: ClasesService
  ) {
    this.segment = 'misClases';
  }

  ngOnInit() {
    this.cargarClases();
    console.log(this.claseService.clasesEnVivo);
  }
  async cargarClases() {
    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Un momento por favor...',
    });
    await loading.present();
    this.claseService.obtenerClases();
    // this.claseService.actualizarListaClases(this.segment);
    loading.dismiss();
    this.datosCargados = true;
  }

  reintentar() {
    this.claseService.obtenerClases();
    // this.claseService.actualizarListaClases(this.segment);
  }
  actualizarClases() {
    // this.errorConexion = this.claseService.errorConexion;
    this.claseService.filtrarClases(this.segment);
  }

  irAClases() {
    this.segment = 'inscribirse';
  }

  verDetalleClase(idClase: number) {
    this.claseService.listaClases.forEach((c) => {
      if (c.idClase === idClase) {
        if (this.segment === 'misClases') c.inscripto = 1;
        this.router.navigate(['/clasesDetalle/' + c.idClase], {
          state: c,
        });
      }
    });
  }

  // async _actualizarListaClases() {
  //   if (this.segment === 'inscribirse') {
  //     this.listaClases = this.clases.filter((clase) => {
  //       return !this.userData.misClases.includes(String(clase.idClase));
  //     });
  //   } else if (this.segment === 'misClases') {
  //     this.listaClases = this.clases.filter((clase) => {
  //       return this.userData.misClases.includes(String(clase.idClase));
  //     });
  //   }
  // }
}
