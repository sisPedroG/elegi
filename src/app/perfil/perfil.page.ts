import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppService } from '../providers/app/app.service';
import { NivelesService } from '../providers/niveles.service';
import { puntos } from '../utils/interfaces';
@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.page.html',
  styleUrls: ['./perfil.page.scss'],
})
export class PerfilPage implements OnInit {
  atributosUsuario: puntos;
  datosCargados: boolean = false;
  nivelGeneral: number = 0;
  segment: string = 'avatar';
  participaciones: any = { eventos: [], clases: [] };
  constructor(
    public nivelesS: NivelesService,
    public service: AppService,
    public router: Router
  ) {}

  async ngOnInit() {}
  ionViewDidEnter() {
    this.atributosUsuario = this.nivelesS.atributosUsuario;

    this.datosCargados = true;
    let xpAcumulada = 0;
    this.participaciones = this.nivelesS.participaciones;
    console.log(this.participaciones);
  }
  navigate() {
    this.router.navigate(['mis-misiones']);
  }
  irADetalle(string) {
    string == 'clases'
      ? this.router.navigate(['participaciones-clases'])
      : this.router.navigate(['participaciones-eventos']);
  }
  ionViewDidLeave() {}
  ionViewWillLeave() {}
  ionViewWillEnter() {}
}
