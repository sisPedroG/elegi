import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
// import format from 'date-fns/format';
@Component({
  selector: 'articulos-carrousel',
  templateUrl: './articulos-carrousel.component.html',
  styleUrls: ['./articulos-carrousel.component.scss'],
})
export class ArticulosCarrouselComponent implements OnInit {
  @Input() data: any;
  @Input() tituloSeccion: string;
  @Output() articuloDetalle = new EventEmitter<number>(); //TODO    CAMBIAR A IR A DETALLE
  @Output() tallerDetalle = new EventEmitter<number>(); //TODO    CAMBIAR A IR A DETALLE
  dateFormateada: string;
  slideOpts = {
    freeMode: true,
    slidesPerView: 2,
    speed: 200,
    loop: true,
    autoPlay: { delay: 2000 },
  };
  constructor() {}

  ngOnInit() {}
  verDetalleArticulo(value: number) {
    this.articuloDetalle.emit(value);
  }
  verDetalleTaller(value: number) {
    this.articuloDetalle.emit(value);
  }
}
