import { Component, OnInit, Input } from '@angular/core';
import { novedadInt } from '../../utils/interfaces';
@Component({
  selector: 'lista-novedades',
  templateUrl: './lista-novedades.component.html',
  styleUrls: ['./lista-novedades.component.scss'],
})
export class ListaNovedadesComponent implements OnInit {
  @Input() listaNovedades: novedadInt[];
  constructor() {}

  ngOnInit() {
  }
}
