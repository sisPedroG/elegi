import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { puntos } from 'src/app/utils/interfaces';

@Component({
  selector: 'mission-modal',
  templateUrl: './mission-modal.component.html',
  styleUrls: ['./mission-modal.component.scss'],
})
export class MissionModalComponent implements OnInit {
  @Input() modalText: string;
  @Input() puntos: puntos;
  @Input() srcIcon: string;
  @Output() ocultarModal = new EventEmitter<void>();
  customIcon: boolean = false;
  constructor() {}

  ngOnInit() {
    console.log(this.srcIcon);
    if (this.srcIcon === undefined) {
    } else {
      this.customIcon = true;
    }
  }
  ocultarModalFn() {
    console.log('emit');
    this.ocultarModal.emit();
  }
}
