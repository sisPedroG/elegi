import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { taller } from '../../utils/interfaces';
@Component({
  selector: 'talleres-activos',
  templateUrl: './talleres-activos.component.html',
  styleUrls: ['./talleres-activos.component.scss'],
})
export class TalleresActivosComponent implements OnInit {
  @Input() talleres: Array<any>;
  @Output() tallerDetalle = new EventEmitter<number>();

  slideOpts = {
    freeMode: true,
    slidesPerView: 2,
    speed: 200,
    loop: true,
    autoPlay: { delay: 2000 },
  };
  constructor() {}

  ngOnInit() {
  }
  verDetalleTaller(value: number) {
    this.tallerDetalle.emit(value);
  }
}
