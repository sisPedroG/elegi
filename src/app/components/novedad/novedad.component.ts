import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { novedadInt } from 'src/app/utils/interfaces';
import formatDistance from 'date-fns/formatDistance';
import { es } from 'date-fns/esm/locale';
@Component({
  selector: 'novedad',
  templateUrl: './novedad.component.html',
  styleUrls: ['./novedad.component.scss'],
})
export class NovedadComponent implements OnInit {
  @Input() novedad: novedadInt;
  @Input() categoriaPrev: string;
  dateParsed: string;
  constructor(public router: Router) {}
  favear() {}
  ngOnInit() {
    console.log(this.novedad);
    let options = { locale: es, addSuffix: true };
    this.dateParsed = formatDistance(
      new Date(this.novedad.fechaDeCreacion),
      new Date(),
      options
    );
    this.dateParsed = this.jsUcfirst(this.dateParsed);
  }
  jsUcfirst(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }
  navigate(c: any) {
    this.router.navigate(['/novedadDetalle/'], {
      state: c,
    });
  }
}
