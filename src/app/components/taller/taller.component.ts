import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { taller } from 'src/app/utils/interfaces';

@Component({
  selector: 'Taller',
  templateUrl: './taller.component.html',
  styleUrls: ['./taller.component.scss'],
})
export class TallerComponent implements OnInit {
  @Input() taller: taller;
  @Output() tallerDetalle = new EventEmitter<number>();

  constructor() {}

  ngOnInit() {
    console.log(this.taller);
    if (this.taller.completo == undefined) this.taller.completo = 0;
  }
  verDetalleTaller(value: number) {
    this.tallerDetalle.emit(value);
  }
}
