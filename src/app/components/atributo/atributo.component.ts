import { Component, Input, OnInit, Type } from '@angular/core';
import { NivelesService } from 'src/app/providers/niveles.service';

@Component({
  selector: 'atributo',
  templateUrl: './atributo.component.html',
  styleUrls: ['./atributo.component.scss'],
})
export class AtributoComponent implements OnInit {
  @Input() puntosExperiencia: any;
  @Input() color: any;
  @Input() icono: any;
  nivel: number;
  experienciaNivel: number;
  progreso: number;
  style: string;

  constructor(public nivelesS: NivelesService) {}
  ngOnInit() {
    this.nivel = this.nivelesS.calcularNivel(
      this.nivelesS.atributosUsuario[this.color]
    );
    // this.experienciaNivel = this.nivelesS.obtenerExperienciaProximoNivel(
    //   this.nivelesS.atributosUsuario[this.color]
    // );
    // let experienciaFaltante =
    //   this.experienciaNivel - this.nivelesS.atributosUsuario[this.color];
    // this.progreso =
    //   (this.nivelesS.EXPERIENCIA_POR_NIVEL - experienciaFaltante) /
    //   this.nivelesS.EXPERIENCIA_POR_NIVEL;
    this.style = `--ion-color-base: var(--ion-color-${this.color}-gradient); --ion-color-base-rgb: var(--ion-color-${this.color}-background); --ion-color-base-rgb: transparent;
    `;

    // let factor1 =
    //   this.puntosExperiencia - this.nivelesS.EXPERIENCIA_POR_NIVEL / this.nivel;
    // let factor2 =
    //   /* eslint no-extra-parens: "error" */
    //   this.experienciaNivel - this.nivelesS.EXPERIENCIA_POR_NIVEL / this.nivel;
    // this.progreso = factor1 / factor2;
  }
  ionViewDidEnter() {}
}
