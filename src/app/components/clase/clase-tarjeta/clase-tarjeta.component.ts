import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { clase } from '../../../utils/interfaces';
@Component({
  selector: 'clase-tarjeta',
  templateUrl: './clase-tarjeta.component.html',
  styleUrls: ['./clase-tarjeta.component.scss'],
})
export class ClaseTarjetaComponent implements OnInit {
  @Input() clase: clase;
  @Output() claseDetalle = new EventEmitter<number>();

  constructor() {}

  ngOnInit() {
  }
  verDetalleClase(value: number) {
    this.claseDetalle.emit(value);
  }
}
