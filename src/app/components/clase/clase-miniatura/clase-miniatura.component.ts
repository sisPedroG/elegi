import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { clase } from '../../../utils/interfaces';
@Component({
  selector: 'clase-miniatura',
  templateUrl: './clase-miniatura.component.html',
  styleUrls: ['./clase-miniatura.component.scss'],
})
export class ClaseMiniaturaComponent implements OnInit {
  @Input() clase: clase;
  @Output() claseDetalle = new EventEmitter<number>();

  constructor() {}

  ngOnInit() {
  }
  corre() {}
  verDetalleClase(value: number) {
    this.claseDetalle.emit(value);
  }
}
