import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ArticulosCarrouselTallerComponent } from './articulos-carrousel-taller.component';

describe('ArticulosCarrouselComponent', () => {
  let component: ArticulosCarrouselTallerComponent;
  let fixture: ComponentFixture<ArticulosCarrouselTallerComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [ArticulosCarrouselTallerComponent],
        imports: [IonicModule.forRoot()],
      }).compileComponents();

      fixture = TestBed.createComponent(ArticulosCarrouselTallerComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    })
  );

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
