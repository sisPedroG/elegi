import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'articulos-carrousel-taller',
  templateUrl: './articulos-carrousel-taller.component.html',
  styleUrls: ['./articulos-carrousel-taller.component.scss'],
})
export class ArticulosCarrouselTallerComponent implements OnInit {
  @Input() data: Array<any>;
  @Output() articuloDetalle = new EventEmitter<number>(); //TODO    CAMBIAR A IR A DETALLE
  @Output() tallerDetalle = new EventEmitter<number>(); //TODO    CAMBIAR A IR A DETALLE
  cantidadVideos: number = 0;
  cantidadVideosVistos: number = 0;
  slideOpts = {
    freeMode: true,
    slidesPerView: 2,
    speed: 200,
    loop: true,
    autoPlay: { delay: 2000 },
  };
  constructor() {}

  ngOnInit() {
    // let talleresTmp = this.data;

    this.data.forEach((taller) => {
      this.cantidadVideos = taller.videos?.length;
      // if (taller.descripcion.length > 40) {
      //   talleresTmp = taller.descripcion.substring(0, 40);
      // }
    });
    console.log(this.data);
  }
  verDetalleTaller(value: number) {
    this.tallerDetalle.emit(value);
  }
}
