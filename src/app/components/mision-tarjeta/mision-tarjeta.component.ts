import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'mision-tarjeta',
  templateUrl: './mision-tarjeta.component.html',
  styleUrls: ['./mision-tarjeta.component.scss'],
})
export class MisionTarjetaComponent implements OnInit {
  @Input() mision: any;
  tipoMision: string;
  icon: string;
  constructor() {}
  ngOnInit() {
    console.log(this.mision);
    this.mision['contador'] == this.mision['metaObjetivo']
      ? (this.icon = '../../assets/icons/mission-checked.svg')
      : (this.icon = '../../assets/icons/mission-unchecked.svg');
  }
}
