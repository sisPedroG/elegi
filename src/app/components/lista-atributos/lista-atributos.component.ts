import { Component, Input, OnInit } from '@angular/core';
import { NivelesService } from 'src/app/providers/niveles.service';
import { puntos } from '../../utils/interfaces';
@Component({
  selector: 'lista-atributos',
  templateUrl: './lista-atributos.component.html',
  styleUrls: ['./lista-atributos.component.scss'],
})
export class ListaAtributosComponent implements OnInit {
  @Input() atributos: puntos;
  atributosUsuarioNivel: puntos;
  verLista: boolean = false;
  constructor(public nivelesS: NivelesService) {}

  ngOnInit() {
    this.atributosUsuarioNivel = this.calcularNiveles(this.atributos);
    // this.verLista = true;
  }

  IonViewWillEnter() {}
  IonViewDidEnter() {}
  calcularNiveles(puntos: puntos) {
    let puntosTemp: puntos = {};
    puntosTemp.bienestar = this.nivelesS.calcularNivel(
      this.nivelesS.atributosUsuario.bienestar
    );
    puntosTemp.creatividad = this.nivelesS.calcularNivel(
      this.nivelesS.atributosUsuario.creatividad
    );
    puntosTemp.conocimiento = this.nivelesS.calcularNivel(
      this.nivelesS.atributosUsuario.conocimiento
    );
    puntosTemp.destreza = this.nivelesS.calcularNivel(
      this.nivelesS.atributosUsuario.destreza
    );
    return puntosTemp;
  }
}
