import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'error-conexion',
  templateUrl: './error-conexion.component.html',
  styleUrls: ['./error-conexion.component.scss'],
})
export class ErrorConexionComponent implements OnInit {
  @Output() reintentar = new EventEmitter<string>();
  constructor() {}

  ngOnInit() {}
  reintentarFn() {
    console.log('rFN');
    this.reintentar.emit('ok');
  }
}
