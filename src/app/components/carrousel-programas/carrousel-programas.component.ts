import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'carrousel-programas',
  templateUrl: './carrousel-programas.component.html',
  styleUrls: ['./carrousel-programas.component.scss'],
})
export class CarrouselProgramasComponent implements OnInit {
  @Input() data: any;
  @Input() tituloSeccion: string;
  @Output() programaDetalle = new EventEmitter<number>(); //TODO    CAMBIAR A IR A DETALLE
  @Output() tallerDetalle = new EventEmitter<number>(); //TODO    CAMBIAR A IR A DETALLE
  dateFormateada: string;
  slideOpts = {
    freeMode: true,
    slidesPerView: 2,
    speed: 200,
    loop: true,
    autoPlay: { delay: 2000 },
  };
  constructor() {}
  ngOnInit() {}
  verDetallePrograma(value: number) {
    this.programaDetalle.emit(value);
  }
}
