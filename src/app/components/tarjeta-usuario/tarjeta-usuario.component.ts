import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserDataService } from 'src/app/providers/user-data.service';
import { AvatarOptions } from '../../../projects/avatar/src/public-api';
import { Clothes } from '../../../projects/avatar/src/public-api';
import { AvatarService } from '../../../projects/avatar/src/lib/avatar.service';
import { NivelesService } from 'src/app/providers/niveles.service';
// import { ViewController } from 'ionic-angular';
@Component({
  selector: 'tarjeta-usuario',
  templateUrl: './tarjeta-usuario.component.html',
  styleUrls: ['./tarjeta-usuario.component.scss'],
})
export class TarjetaUsuarioComponent implements OnInit {
  showAngular = false;
  showImage = false;
  showSvg = false;
  svgData: string;
  clothesEnum = Clothes;

  constructor(
    public router: Router,
    public userData: UserDataService,
    public avatarService: AvatarService,
    public nivelesS: NivelesService
  ) {}

  async ngOnInit() {
    // this.options = this.avatarService.avatar;
    // this.avatarForm.valueChanges.subscribe((value) => {
    //   this.options = value;
    //   setTimeout(() => {
    //     return this.toggleSvg(false);
    //   }, 0);
    //   this.showColourFabric();
    // });
    // this.options = await this.userData.getAvatar();
  }
  showColourFabric() {
    if (
      this.avatarService.avatar.clothes === this.clothesEnum.BLAZER_SHIRT ||
      this.avatarService.avatar.clothes === this.clothesEnum.BLAZER_SWEATER
    ) {
      return false;
    }
    return true;
  }
  toggleSvg(bool) {
    if (bool) {
      this.showSvg = !this.showSvg;
      this.showAngular = false;
      this.showImage = false;
    }
    const svgNode = document.getElementById('svgid');
    this.svgData = svgNode.innerHTML;
  }
  cambiarAvatar() {
    const svgNode = document.getElementById('svgid');
    svgNode.innerHTML = '';
    // this.options = null;
    this.router.navigate(['/avatar'], {
      state: { paginaPrev: 'perfil' },
    });
  }
  ionViewWillEnter() {}
  ionViewWillLeave() {
    console.log('leaves')
  }  ionViewDidLeave() {
    console.log('leaves')
  }
}
