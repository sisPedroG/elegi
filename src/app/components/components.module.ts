import {
  CUSTOM_ELEMENTS_SCHEMA,
  NgModule,
  NO_ERRORS_SCHEMA,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { NovedadComponent } from './novedad/novedad.component';
import { ListaNovedadesComponent } from './lista-novedades/lista-novedades.component';
import { ClaseTarjetaComponent } from './clase/clase-tarjeta/clase-tarjeta.component';
import { ClaseMiniaturaComponent } from './clase/clase-miniatura/clase-miniatura.component';
import { TallerComponent } from './taller/taller.component';
import { TarjetaUsuarioComponent } from './tarjeta-usuario/tarjeta-usuario.component';
import { TalleresActivosComponent } from './talleres-activos/talleres-activos.component';
import { YtPlayerComponent } from './yt-player/yt-player.component';
import { ListaAtributosComponent } from './lista-atributos/lista-atributos.component';
import { AtributoComponent } from './atributo/atributo.component';
import { AvatarComponent } from '../../projects/avatar/src/lib/avatar.component';
import { ArticulosCarrouselComponent } from './articulos-carrousel/articulos-carrousel.component';
import { ArticulosCarrouselTallerComponent } from './articulos-carrousel-taller/articulos-carrousel-taller.component';
import { ErrorConexionComponent } from './error-conexion/error-conexion.component';
import { LogroComponent } from './logro/logro.component';
import { CarrouselProgramasComponent } from './carrousel-programas/carrousel-programas.component';
import { MisionTarjetaComponent } from './mision-tarjeta/mision-tarjeta.component';
import { MissionModalComponent } from './mission-modal/mission-modal.component';
@NgModule({
  declarations: [
    NovedadComponent,
    ListaNovedadesComponent,
    ClaseTarjetaComponent,
    ClaseMiniaturaComponent,
    TallerComponent,
    TarjetaUsuarioComponent,
    TalleresActivosComponent,
    YtPlayerComponent,
    ListaAtributosComponent,
    AtributoComponent,
    AvatarComponent,
    ArticulosCarrouselComponent,
    ArticulosCarrouselTallerComponent,
    CarrouselProgramasComponent,
    ErrorConexionComponent,
    LogroComponent,
    MisionTarjetaComponent,
    MissionModalComponent,
  ],
  imports: [CommonModule],
  exports: [
    NovedadComponent,
    ListaNovedadesComponent,
    ClaseTarjetaComponent,
    ClaseMiniaturaComponent,
    TallerComponent,
    TarjetaUsuarioComponent,
    TalleresActivosComponent,
    YtPlayerComponent,
    ListaAtributosComponent,
    AtributoComponent,
    AvatarComponent,
    ArticulosCarrouselComponent,
    ArticulosCarrouselTallerComponent,
    CarrouselProgramasComponent,
    ErrorConexionComponent,
    LogroComponent,
    MisionTarjetaComponent,
    MissionModalComponent,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
})
export class ComponentsModule {}
