import { Component, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { IonTabs, MenuController } from '@ionic/angular';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss'],
})
export class TabsPage {
  @ViewChild('tabs', { static: false }) tabs: IonTabs;
  tabSeleccionada = '';
  pages: Array<any> = [
    {
      url: '/tabs/novedad',
      title: 'puneja',
    },
    {
      url: '/tabs/roadtrips',
      title: 'puneja',
    },
    {
      url: '/tabs/perfil',
      title: 'puneja',
    },
  ];
  menuAbierto: boolean;
  constructor(public menuController: MenuController, public router: Router) {}
  ngAfterViewInit() {
    this.tabSeleccionada = this.tabs.getSelected();
  }
  setCurrentTab() {
    this.tabSeleccionada = this.tabs.getSelected();
  }
  openSideMenu() {
    this.menuAbierto = true;
    this.menuController.open();
  }
  navigate(url) {
    this.router.navigate([url]);
  }
  navegar() {
    this.router.navigate(['/escanerQR']);
  }
}
