import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'novedades',
        loadChildren: () =>
          import('../novedades/novedades.module').then(
            (m) => m.NovedadesPageModule
          ),
      },
      {
        path: 'clases',
        loadChildren: () =>
          import('../clases/clases.module').then((m) => m.ClasesPageModule),
      },
      {
        path: 'roadtrips',
        loadChildren: () =>
          import('../roadtrips/roadtrips.module').then(
            (m) => m.RoadtripsPageModule
          ),
      },
      {
        path: 'perfil',
        loadChildren: () =>
          import('../perfil/perfil.module').then((m) => m.PerfilPageModule),
      },
      {
        path: 'mas',
        loadChildren: () =>
          import('../mas/mas.module').then((m) => m.MasPageModule),
      },
      {
        path: '',
        redirectTo: '/tabs/novedades',
        pathMatch: 'full',
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class TabsPageRoutingModule {}
