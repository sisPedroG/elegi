export interface novedadInt {
  idArticulo?: number;
  titulo?: string;
  destacado?: string;
  texto?: string;
  imagen?: string;
  link?: string;
  categoria?: string;
  likes?: number;
  fechaDeCreacion?: string;
  exclusivoUG?: Array<number>;
  comoFormarParte?: string;
  descripcion?: string;
}
export interface eventos {
  idEvento?: number;
  titulo?: string;
  cuerpoEvento?: string;
  imagen?: string;
  formulario?: string;
  categoria?: string;
  likes?: number;
  fechaDeCreacion?: string;
  comoFormarParte?: string;
  descripcion?: string;
  puntos?: puntos;
}

export interface horario {
  dia: number;
  horario: string;
}
export interface clase {
  idClase?: number;
  titulo?: string;
  descripcion?: string;
  horarios?: Array<any>;
  horario?: string;
  imagen?: string;
  imagenMini?: string;
  enVivo?: boolean;
  inscripto?: number;
  sobreLaClase?: string;
  horarioCompleto?: Array<string>;
  masInformacion?: string;
  puntos?: puntos;
  autor?: string;
  link?: string;
  formulario?: string;
}
export interface logros {
  idLogro: number;
  titulo: string;
  descripcion: string;
}
export interface puntos {
  conocimiento?: number;
  bienestar?: number;
  creatividad?: number;
  destreza?: number;
}
export interface video {
  idTallerVideos?: number;
  duracion?: string;
  thumbnail?: string;
  imagen?: string;
  vistoPorElUsuario?: boolean;
  urlVideo?: string;
  visto?: number;
  notasDelVideo?: string;
  tituloVideo?: string;
  conocimiento?: number;
  bienestar?: number;
  creatividad?: number;
  destreza?: number;
}
export interface taller {
  idTaller?: number;
  titulo?: string;
  imagen?: string;
  miniImagen?: string;
  activo?: boolean;
  descripcion?: string;
  videos?: Array<video>;
  puntos?: puntos;
  vistoPorElUsuario?: boolean;
  autor?: string;
  texto?: string;
  nombreCategoria?: string;
  completo?: number;
  videosVistos?: number;
}
