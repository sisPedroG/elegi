import { Location } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import {
  NavController,
  LoadingController,
  Platform,
  MenuController,
} from '@ionic/angular';
import { AppService } from '../providers/app/app.service';
import { NivelesService } from '../providers/niveles.service';
import { puntos } from '../utils/interfaces';
@Component({
  selector: 'app-trivia',
  templateUrl: './trivia.page.html',
  styleUrls: ['./trivia.page.scss'],
})
export class TriviaPage implements OnInit {
  public timer;
  comenzar: boolean = false;
  idTrivia_pregunta = '';
  idTrivia_opcion = '';
  preguntaAnterior: any = '';
  respuestasAnteriores: any = [];
  mostrarTrivia = 0;
  envioRespuesta: boolean = false;

  pregunta = '';
  mensaje = '';
  mensajeMisionTrivia = '';
  idRespuestaAnterior: string = '';
  respuesta_items: Array<any> = [];
  pregunta_items: Array<any> = [];
  continuar: boolean = true;
  preguntaRespondida: boolean = false;
  respuestagrabada: boolean = false;
  mostrarResultados: number = 0;
  mensajeResultados: string = '';
  correctas: string = '';
  total: string = '';
  porcentaje: string = '';
  nombreArchivo: string = '';
  errorConexion: boolean = false;
  intervalVar: any;
  tematicaTrivia: string = '';
  idTrivia: number;
  ocultarModal: boolean = false;
  puntos: puntos;
  imagenBackground: string;
  constructor(
    public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    public platform: Platform,
    public serviceApp: AppService,
    public menu: MenuController,
    public location: Location,
    public nivelesService: NivelesService
  ) {}

  ngOnInit() {
    // this.imagenBackground = '../../assets/images/trivia/trivia-1.webp';
  }

  ionViewWillEnter() {
    this.platform.ready().then(() => {
      //Populate the form with the current location data
      this.mostrarPregunta();
    });
  }

  salir() {
    // Limpio los datos para que no se mande ninguna respuesta pendiente ya que al salir voluntariamente no debería haber
    localStorage.setItem('triviaTimer', '');
    localStorage.setItem('triviaPregunta', '');
    localStorage.setItem('triviaOpcion', '');
    this.menu.enable(true);
    // this.navCtrl.setRoot(TriviaPage);
  }

  resetTimer() {
    this.timer = 30;
  }

  startTimer(intervalVar) {
    localStorage.setItem('triviaTimer', '1');

    this.intervalVar = setInterval(
      function () {
        if (this.timer > 0) {
          this.timer--;
        } else if (this.timer == 0) {
          this.enviarRespuesta(
            this.pregunta_items[0].idTrivia_pregunta,
            this.pregunta_items[0].idTrivia_opcion
          );
          this.stopTimer();
        }
      }.bind(this),
      1000
    );
  }

  stopTimer() {
    clearInterval(this.intervalVar);
  }

  comenzarTrivia() {
    this.comenzar = true; // Da problemas y desfasa la respuesta. Comenzar llamando a this.mostrarPregunta()
    this.resetTimer();

    this.startTimer(this.intervalVar);
    this.menu.enable(false);
  }

  siguientePregunta() {
    this.resetTimer();
    this.continuar = true;
    this.preguntaRespondida = false;
    this.respuestasAnteriores = [];
    this.startTimer(this.intervalVar);
  }
  async presentLoading() {
    const loading = await this.loadingCtrl.create({
      cssClass: 'my-custom-class',
      message: 'Please wait...',
      duration: 2000,
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();
  }
  enviarRespuesta(pregunta, opcion, idTrivia) {
    this.stopTimer();
    this.envioRespuesta = true;
    this.idTrivia_pregunta = pregunta; // Cargo los parametros para la funcion del provider
    this.idTrivia_opcion = opcion;
    idTrivia == undefined ? (this.idTrivia = 0) : (this.idTrivia = idTrivia);
    this.preguntaAnterior = this.pregunta_items[0].pregunta;
    for (let i = 0; this.pregunta_items?.length > i; i++) {
      // Cargo sus opciones antes de buscar la nueva pregunta
      this.respuestasAnteriores.push({
        idTrivia_opcion: this.pregunta_items[i].idTrivia_opcion,
        opcion: this.pregunta_items[i].opcion,
        respuestaAnterior: false,
        respuestaCorrecta: false,
        idTrivia: this.idTrivia,
      });
    }
    this.mostrarPregunta(); // Envio idTrivia_pregunta y idTrivia_opcion y espero respuesta

    // Create the loading indicator
    this.continuar = false;
    this.preguntaRespondida = true;
    this.idRespuestaAnterior = opcion;
  }

  async mostrarPregunta() {
    this.pregunta_items = [];
    this.mostrarResultados = 0;

    this.presentLoading();

    // Si cerré la app y no contesté la pregunta y ahora no hay pregunta cargada la tengo que enviar. El Timer quedó activo.
    if (
      localStorage.getItem('triviaTimer') &&
      this.idTrivia_pregunta == '' &&
      localStorage.getItem('triviaPregunta')
    ) {
      this.idTrivia_pregunta = localStorage.getItem('triviaPregunta');
      this.idTrivia_opcion = localStorage.getItem('triviaOpcion');
    }

    this.serviceApp
      .trivia(this.idTrivia_pregunta, this.idTrivia_opcion, this.idTrivia)
      .then(
        (data) => {
          this.loadingCtrl.dismiss();
          if (data) {
            this.respuesta_items = this.formatRespuesta(data.respuesta);
            this.verificarRespuesta();
            if (data.puntos['creatividad'] != undefined) {
              this.ocultarModal = true;
              this.puntos = data.puntos;
              this.mensajeMisionTrivia = data.mensaje;
            }
            this.pregunta_items = this.formatPregunta(data.pregunta);
            this.tematicaTrivia = data.pregunta[0]['tematicaTrivia'];
            this.mostrarResultados = data.resultados[0]?.mostrarResultados;
            localStorage.setItem('triviaTimer', '');
            localStorage.setItem(
              'triviaPregunta',
              this.pregunta_items[0].idTrivia_pregunta
            );
            localStorage.setItem(
              'triviaOpcion',
              this.pregunta_items[0].idTrivia_opcion
            );

            // CHEQUEO SI TENGO PUNTOS
            if (this.mostrarResultados == 1) {
              this.correctas = data.resultados[0].correctas;
              this.total = data.resultados[0].total;
            }

            this.pregunta = this.pregunta_items[0].pregunta;
            if (this.pregunta_items[0].mostrarTrivia == 0) {
              this.menu.enable(true);
            }

            this.mensaje = this.pregunta_items[0].mensaje;
            this.mostrarTrivia = this.pregunta_items[0].mostrarTrivia;
            this.errorConexion = false;
          } else {
            //This really should never happen
            this.errorConexion = true;
          }
          this.imagenBackground = data['imagen'];
        },
        (error) => {
          //Hide the loading indicator
          this.errorConexion = true;
          this.loadingCtrl.dismiss();
        }
      );
  }

  verificarRespuesta() {
    for (let i = 0; this.respuestasAnteriores?.length > i; i++) {
      if (
        this.respuestasAnteriores[i].idTrivia_opcion == this.idRespuestaAnterior
      ) {
        this.respuestasAnteriores[i].respuestaAnterior = true;
      }
      if (
        this.respuestasAnteriores[i].idTrivia_opcion ==
        this.respuesta_items[0].opcionCorrecta
      ) {
        this.respuestasAnteriores[i].respuestaCorrecta = true;
      }
    }
  }
  async volver() {
    if (this.envioRespuesta) {
      const loading = await this.loadingCtrl.create({
        cssClass: 'my-custom-class',
        message: 'Un momento por favor...',
      });
      await loading.present();
      // this.claseService.actualizarListaClases(this.segment);
      await this.nivelesService.obtenerAtributos();
      loading.dismiss();
    }
    this.location.back();
  }
  ocultarModalFn() {
    this.ocultarModal = false;
    this.puntos = {};
  }
  private formatRespuesta(items): any {
    let tmpArray = [];

    //Veo si encontro algun registro
    if (items) {
      // Recorro
      for (let i = 0; i < items?.length; i++) {
        tmpArray.push({
          opcionCorrecta: items[i].opcionCorrecta,
        });
      }
    }

    // Devuelvo la array
    return tmpArray;
  }

  private formatPregunta(items): any {
    let tmpArray = [];

    //Veo si encontro algun registro
    if (items) {
      // Recorro
      for (let i = 0; i < items?.length; i++) {
        tmpArray.push({
          mostrarTrivia: items[i].mostrarTrivia,
          tematicaTrivia: items[i].tematicaTrivia,
          mensaje: items[i].mensaje,
          idTrivia_opcion: items[i].idTrivia_opcion,
          idTrivia_pregunta: items[i].idTrivia_pregunta,
          opcion: items[i].opcion,
          pregunta: items[i].pregunta,
          idTrivia: items[i].idTrivia,
        });
      }
    }

    // Devuelvo la array
    return tmpArray;
  }
}
