import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { UserDataService } from '../providers/user-data.service';
@Component({
  selector: 'app-mas',
  templateUrl: './mas.page.html',
  styleUrls: ['./mas.page.scss'],
})
export class MasPage implements OnInit {
  constructor(
    public router: Router,
    public userData: UserDataService,
    public alertCtrl: AlertController
  ) {}

  ngOnInit() {}
  navigate(pagina: string) {
    if (pagina == 'logout') {
      this.userData.logout();
    } else {
      pagina !== undefined ? this.router.navigate([pagina]) : 0;
    }
  }
  irABiblioteca() {
    this.router.navigateByUrl('https://elegi-bienestar.com/');
  }
  async presentAlert() {
    const alert = await this.alertCtrl.create({
      header: 'Atención',
      message: `¿Estás seguro que quieres salir de la app?`,
      buttons: [
        {
          text: 'Cancelar',
          cssClass: 'secondary',
          handler: (blah) => {},
        },
        {
          text: 'Aceptar',
          cssClass: 'primary',
          handler: () => {
            // this.router.navigateByUrl['https://elegi-bienestar.com/'];
            window.location.href = 'https://elegi-bienestar.com/';
          },
        },
      ],
    });

    await alert.present();
  }
}
