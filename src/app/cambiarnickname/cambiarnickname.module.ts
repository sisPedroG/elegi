import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CambiarnicknamePageRoutingModule } from './cambiarnickname-routing.module';

import { CambiarnicknamePage } from './cambiarnickname.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CambiarnicknamePageRoutingModule
  ],
  declarations: [CambiarnicknamePage]
})
export class CambiarnicknamePageModule {}
