import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CambiarnicknamePage } from './cambiarnickname.page';

const routes: Routes = [
  {
    path: '',
    component: CambiarnicknamePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CambiarnicknamePageRoutingModule {}
