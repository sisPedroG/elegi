import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { AppService } from '../providers/app/app.service';
import { UserDataService } from '../providers/user-data.service';

@Component({
  selector: 'app-cambiarnickname',
  templateUrl: './cambiarnickname.page.html',
  styleUrls: ['./cambiarnickname.page.scss'],
})
export class CambiarnicknamePage implements OnInit {
  nickname: string = '';
  constructor(
    public service: AppService,
    public router: Router,
    public toastController: ToastController,
    public userData: UserDataService
  ) {}

  ngOnInit() {}
  enviarNickname() {
    if (this.nickname?.length == 0) {
      this.presentToast('Ingresá un nickname.');
    } else {
      this.userData.nickname = this.nickname;
      this.userData.setNickname(this.nickname).then(() => {
        this.service.enviarNickname(this.nickname).then((data) => {
          this.router.navigate(['/avatar']);
        });
      });
    }
  }
  async presentToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000,
      color: 'danger',
    });
    toast.present();
  }
}
