import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RoadtripDetallePageRoutingModule } from './roadtrip-detalle-routing.module';

import { RoadtripDetallePage } from './roadtrip-detalle.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RoadtripDetallePageRoutingModule
  ],
  declarations: [RoadtripDetallePage]
})
export class RoadtripDetallePageModule {}
