import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RoadtripDetallePage } from './roadtrip-detalle.page';

const routes: Routes = [
  {
    path: '',
    component: RoadtripDetallePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RoadtripDetallePageRoutingModule {}
