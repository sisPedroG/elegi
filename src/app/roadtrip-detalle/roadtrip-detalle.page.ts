import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { taller } from '../utils/interfaces';

@Component({
  selector: 'app-roadtrip-detalle',
  templateUrl: './roadtrip-detalle.page.html',
  styleUrls: ['./roadtrip-detalle.page.scss'],
})
export class RoadtripDetallePage implements OnInit {
  constructor(private router: Router, public location: Location) {}
  taller: taller = {};
  ngOnInit() {
    this.taller = this.router.getCurrentNavigation().extras.state;
  }

  ionViewWillEnter() {}
  volver() {
    this.location.back();
  }
  ionViewWillLeave() {}
  irDetalleVideo(video) {
    this.router.navigate(['/roadtrip-video'], {
      state: { video: video, idTaller: this.taller.idTaller },
    });
  }
}
