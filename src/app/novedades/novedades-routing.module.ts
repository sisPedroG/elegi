import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { novedadesPage } from './novedades.page';

const routes: Routes = [
  {
    path: '',
    component: novedadesPage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class novedadesPageRoutingModule {}
