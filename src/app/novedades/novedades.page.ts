import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController, MenuController } from '@ionic/angular';
import { AppService } from '../providers/app/app.service';
import { UserDataService } from '../providers/user-data.service';
import { novedadInt, eventos } from '../utils/interfaces';
@Component({
  selector: 'app-novedades',
  templateUrl: 'novedades.page.html',
  styleUrls: ['novedades.page.scss'],
})
export class novedadesPage {
  novedadesArray: Array<novedadInt> = [];
  errorConexion: boolean = false;
  novedadDestacada: novedadInt;
  eventosArray: Array<eventos> = [];
  programasArray: Array<any> = [];
  // fechaDescatadoTmp:date;
  constructor(
    private service: AppService,
    public userData: UserDataService,
    public router: Router, // public ava: avatars // public avat: avatar
    public menu: MenuController,
    public loadingCtrl: LoadingController
  ) {}
  ngOnInit() {
    this.actualizarDatos();
  }
  async actualizarDatos() {
    const loading = await this.loadingCtrl.create({
      cssClass: 'my-custom-class',
      message: 'Please wait...',
      duration: 2000,
    });
    await loading.present();
    console.log('actualizar datos');
    this.presentLoading();
    this.service.obtenerArticulos().then(
      (data) => {
        loading.dismiss();
        console.log('data');
        if (data != null || data != undefined) {
          this.novedadesArray = data['novedades'];
          this.eventosArray = data['eventos'];
          this.programasArray = data['programas'];
          this.errorConexion = false;
          console.log('data');
          console.log(data);
          console.log(this.programasArray);
          console.log(this.eventosArray);
          if (this.novedadesArray == null) {
            this.errorConexion = true;
          }
          this.novedadDestacada = this.novedadesArray.find(
            (novedad) => novedad.destacado == '1'
          );
          this.novedadesArray = this.novedadesArray.filter(
            (novedad) => novedad.idArticulo != this.novedadDestacada.idArticulo
          );
        }
      },
      (err) => {
        console.log(err);
        this.errorConexion = true;
      }
    );
    this.menu.enable(true);
  }
  async presentLoading() {
    // const { role, data } = await loading.onDidDismiss();
  }
  reintentar() {
    this.actualizarDatos();
  }

  logout() {
    this.userData.logout();
  }
  verDetallePrograma(a: any) {
    this.programasArray.forEach((n) => {
      if (n.idPrograma === a) {
        this.router.navigate(['/novedadDetalle/'], {
          state: n,
        });
      }
    });
  }
  verDetalleArticulo(a: any) {
    this.eventosArray.forEach((n) => {
      if (n.idEvento === a) {
        this.router.navigate(['/novedadDetalle/'], {
          state: n,
        });
      }
    });
  }
  //  verDetalleClase(idClase: number) {
  //   this.claseService.listaClases.forEach((c) => {
  //     if (c.idClase === idClase) {
  //       if (this.segment === 'misClases') c.inscripto = true;
  //       this.router.navigate(['/clasesDetalle/' + c.idClase], {
  //         state: c,
  //       });
  //     }
  //   });
  // }
}
