import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { novedadesPage } from './novedades.page';
import { novedadesPageRoutingModule } from './novedades-routing.module';
import { ComponentsModule } from '../components/components.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    novedadesPageRoutingModule,
    ComponentsModule,
  ],
  declarations: [novedadesPage],
})
export class NovedadesPageModule {}
