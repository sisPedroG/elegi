import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RoadtripsPage } from './roadtrips.page';

const routes: Routes = [
  {
    path: '',
    component: RoadtripsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RoadtripsPageRoutingModule {}
