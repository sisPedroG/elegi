import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { TalleresService } from '../providers/talleres.service';
import { taller } from '../utils/interfaces';
@Component({
  selector: 'app-roadtrips',
  templateUrl: './roadtrips.page.html',
  styleUrls: ['./roadtrips.page.scss'],
})
export class RoadtripsPage implements OnInit {
  talleresPorCategoria: any = {};
  talleresData: Array<taller> = [];
  talleres: Array<taller> = [];
  talleresEnCurso: Array<taller> = [];
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    public talleresService: TalleresService,
    public loadingController: LoadingController
  ) {}

  async ngOnInit() {
    this.cargarTalleres();
  }
  async cargarTalleres() {
    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Un momento por favor...',
    });
    await loading.present();
    this.talleresService.obtenerTalleres();
    // this.claseService.actualizarListaClases(this.segment);
    loading.dismiss();
    // this.datosCargados = true;
    // let data = await this.service.obtenerTalleres();
    // this.talleresData = data['talleres'];
    // this.ordenarTalleres(this.talleresData);
  }
  reintentar() {
    this.talleresService.obtenerTalleres();
    this.talleresService.ordenarTalleres();
  }

  actualizarTalleres() {
    this.talleresService.ordenarTalleres();
  }
  verDetalleTaller(idTaller: number) {
    this.talleresService.talleresData.forEach((t) => {
      if (t.idTaller === idTaller) {
        this.navigate(t);
      }
    });
  }
  navigate(t: any) {
    this.router.navigate(['/roadtripDetalle/' + t.idTaller], {
      relativeTo: this.route,
      state: t,
    });
  }
}
