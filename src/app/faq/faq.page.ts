import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { AppService } from '../providers/app/app.service';

@Component({
  selector: 'app-faq',
  templateUrl: './faq.page.html',
  styleUrls: ['./faq.page.scss'],
})
export class FaqPage implements OnInit {
  data: Array<any>;
  constructor(public location: Location, public service: AppService) {}

  async ngOnInit() {
    let data = await this.service.obtenerFaq();
    this.data = data['faq'];
  }
  volver() {
    this.location.back();
  }
}
