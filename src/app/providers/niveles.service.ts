import { Injectable } from '@angular/core';
import { puntos } from '../utils/interfaces';
import { AppService } from './app/app.service';
import formatDistance from 'date-fns/formatDistance';
import { es } from 'date-fns/esm/locale';

@Injectable({
  providedIn: 'root',
})
export class NivelesService {
  atributosUsuario: puntos = {
    destreza: 0,
    conocimiento: 0,
    bienestar: 0,
    creatividad: 0,
  };
  progresoAtributosUsuario: puntos = {
    destreza: 0,
    conocimiento: 0,
    bienestar: 0,
    creatividad: 0,
  };
  misiones: any = [];
  participaciones: any = { eventos: [], clases: [] };
  mostrarAtributos: boolean = false;
  xpAcumulada: number = 0;
  EXPERIENCIA_POR_NIVEL = 400;
  nivelGeneral: number = 0;
  experienciaParaSiguienteNivel: number;
  constructor(public service: AppService) {
    this.obtenerAtributos();
  }
  obtenerAtributos() {
    this.service.obtenerPuntosAtributos().then((data) => {
      console.log(data);
      let atributos = data['atributos'];
      Object.keys(atributos).forEach((atributo) => {
        this.atributosUsuario[atributo] = Number(atributos[atributo]);
        console.log(this.atributosUsuario[atributo]);
      });
      this.participaciones['eventos'] = data['eventos'];
      this.participaciones['clases'] = data['clases'];
      this.misiones = data['misiones'];
      this.formatParticipaciones();
      this.formatMisiones();
      this.mostrarAtributos = true;
      this.nivelGeneral = 0;
      Object.keys(this.atributosUsuario).forEach((atributo) => {
        //CALCULO EL NIVEL SUMANDO EL NIVEL DE CADA ATRIBUTO
        this.nivelGeneral += this.calcularNivel(
          this.atributosUsuario[atributo]
        );
        this.calcularProgresoAtributo(atributo);
      });
      console.log(this.progresoAtributosUsuario);
    });
  }
  formatMisiones() {
    let options = { locale: es };
    this.misiones.forEach((mision) => {
      let dateParsed = formatDistance(
        new Date(mision.fechaHasta.replace(' ', 'T')),
        new Date(),
        options
      );
      mision.fechaHasta = this.jsUcfirst(dateParsed);
      console.log(mision.fechaHasta);
    });
  }
  formatParticipaciones() {
    let options = { locale: es };
    this.participaciones['eventos'].forEach((evento) => {
      let dateParsed = formatDistance(
        new Date(),
        new Date(evento.fecha.replace(' ', 'T')),
        options
      );
      evento.fecha = this.jsUcfirst(dateParsed);
    });
    this.participaciones['clases'].forEach((clase) => {
      let dateParsed = formatDistance(
        new Date(),
        new Date(clase.fecha.replace(' ', 'T')),
        options
      );
      clase.fecha = this.jsUcfirst(dateParsed);
    });
  }
  jsUcfirst(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

  calcularNivel(puntos: number): number {
    return Math.floor(puntos / this.EXPERIENCIA_POR_NIVEL);
  }
  actualizarExperiencia(puntos: puntos) {
    Object.keys(this.atributosUsuario).forEach((atributo) => {
      this.atributosUsuario[atributo] += puntos[atributo];
      this.calcularProgresoAtributo(atributo);
    });
  }
  calcularProgresoAtributo(atributo) {
    this.experienciaParaSiguienteNivel = this.obtenerExperienciaProximoNivel(
      //OBTENGO LA XP NECESARIA PARA SUBIR AL PROXIMO NIVEL
      this.atributosUsuario[atributo]
    );
    let experienciaFaltante =
      this.experienciaParaSiguienteNivel - this.atributosUsuario[atributo]; //OBTENGO LA XP FALTANTE RESTANDO LA XP DEL PROXIMO NIVEL Y LA AC TUAL
    this.progresoAtributosUsuario[atributo] =
      (this.EXPERIENCIA_POR_NIVEL - experienciaFaltante) / //CALCULO
      this.EXPERIENCIA_POR_NIVEL;
  }
  // subeDeNivel(puntos: number, experiencia: number): boolean {
  //   if (
  //     Math.floor((puntos + experiencia) / this.EXPERIENCIA_POR_NIVEL) >
  //     Math.floor(puntos / this.EXPERIENCIA_POR_NIVEL)
  //   )
  //     return true;
  //   else return false;
  // }
  subeDeNivel(puntos: puntos): boolean {
    let nivelGeneralTmp = 0;
    Object.keys(puntos).forEach((atributo) => {
      nivelGeneralTmp += this.calcularNivel(
        puntos[atributo] + this.atributosUsuario[atributo]
      );
    });
    if (nivelGeneralTmp > this.nivelGeneral) {
      this.nivelGeneral = nivelGeneralTmp;
      return true;
    } else return false;
  }
  obtenerExperienciaProximoNivel(experienciaActual) {
    return (
      this.EXPERIENCIA_POR_NIVEL *
      (Math.floor(experienciaActual / this.EXPERIENCIA_POR_NIVEL) + 1)
    );
  }
}

// Provider
// Calcular nivel dado un valor de atributo d0ne
// Subir de nivel d0ne

// Servidor
// Registrar Puntos done
// Puntos de experiencia por atributos d0ne
