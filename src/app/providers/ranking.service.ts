import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AppService } from './app/app.service';
export interface ranking{
  general?:Array<any>,
  bienestar?:Array<any>,
  creatividad?:Array<any>,
  conocimiento?: Array<any>,
  destreza?: Array<any>

}
@Injectable({
  providedIn: 'root'
})
export class RankingService {
  // rankingData: ranking = {general:[],bienestar:[],creatividad:[],conocimiento:[],destreza:[]};
  rankingData: Array<any> = [];
  ranking: any = {};
  styleRank:string = '';
  styleBanner:string = '';
  constructor(
    public http: HttpClient,
    public service: AppService
  ) { }
  
  async cargarRanking(categoria){

    const svgNode = document.getElementById('svgid');
    svgNode.innerHTML = ''; // FIX PARA EL AVATAR SUPERPUESTO
    if(this.rankingData[categoria]===undefined){
      (await this.obtenerRanking(categoria)).subscribe(data=>{
        this.styleRank = `rank-banner-${categoria}`;
        this.styleBanner= `lvl-banner-${categoria}`;
        console.log(this.styleRank)
        this.rankingData[categoria] = this.formatData(data);
        this.ranking = this.rankingData[categoria];
        console.log(data);
        console.log(this.rankingData);

      })
      
    }else{
      this.ranking = this.rankingData[categoria];
      this.styleRank = `rank-banner-${categoria}`;
      this.styleBanner= `lvl-banner-${categoria}`;

    }


  }
  async cargarMasPuestosRanking(categoria){
    (await this.obtenerMasPuestosRanking(categoria)).subscribe(data=>{
      console.log(data)
      console.log(this.rankingData[categoria])
      let tmpArray = this.formatData(data);
      tmpArray.ranking.forEach(rank => {
        this.rankingData[categoria].ranking.push(rank);
        
      });
      console.log(this.rankingData)
    })

  }
  async obtenerRanking(categoria: string) {

    let url: string = this.service.servicioEndpoint + '/obtenerranking.php';
    let body = new FormData();
    let token = await this.service.obtenerToken();
    body.append('token', token);
    body.append('categoria',categoria)
    body.append('versionApp', this.service.versionApp);
    return this.http.post<Array<any>>(url, body);
  }

  async obtenerMasPuestosRanking(categoria: string) {
    
    let url: string = this.service.servicioEndpoint + '/obtenermaspuestosranking.php';
    let body = new FormData();
    let token = await this.service.obtenerToken();
    body.append('token', token);
    body.append('categoria',categoria);
    body.append('indice',this.rankingData[categoria].ranking.length);
    body.append('versionApp', this.service.versionApp);
    return this.http.post<Array<any>>(url, body);
  }

  formatData(data){
    let tmpData = data;
    if(data.usuario!==undefined)  tmpData.usuario.puntos=Math.floor(data.usuario.puntos/400);
    tmpData.ranking.forEach(rank => {
      rank.puntos = Math.floor(rank.puntos/400)
    });
    return tmpData;
  }
}
