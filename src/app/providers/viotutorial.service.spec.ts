import { TestBed } from '@angular/core/testing';

import { ViotutorialService } from './viotutorial.service';

describe('ViotutorialService', () => {
  let service: ViotutorialService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ViotutorialService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
