import { Injectable } from '@angular/core';
import { CanLoad, Router } from '@angular/router';
import { UserDataService } from './user-data.service';
@Injectable({
  providedIn: 'root',
})
export class CheckTutorial implements CanLoad {
  constructor(private userData: UserDataService, private router: Router) {}

  canLoad() {
    return this.userData.vioTutorial().then((res) => {
      if (res) {
        this.router.navigate(['/tabs/novedades']);
        return false;
      } else {
        return true;
      }
    });
  }
}
