import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Plugins } from '@capacitor/core';
import { AlertController } from '@ionic/angular';
const { Storage } = Plugins;
@Injectable({
  providedIn: 'root',
})
export class UserDataService {
  NUMERO_DNI = 'numeroDNI';
  HAS_LOGGED_IN = 'hasLoggedIn';
  NOMBRE_USUARIO = 'nombreUsuario';
  FCM_NOMBRE_UG = 'fcmNombreUG';
  TOKEN = 'token';
  ID_USUARIO = 'idUsuario';
  VIO_TUTORIAL = 'vioTutorial';
  ID_CLASES_FAVORITAS = 'idClasesFavoritas';
  NICKNAME = 'nickname';
  AVATAR = 'avatar';
  misClases: string = '';
  token: string = '';
  nickname: string = '';
  PLAYER_ID_ONE_SIGNAL = 'playerIdOneSignal';
  constructor(public alertCtrl: AlertController, public router: Router) {
    this.getToken();
    this.getNickname().then((data) => {
      this.nickname = data.value;
    });
  }

  //LOGIN Y LOGOUT
  isLoggedIn(): Promise<boolean> {
    return Storage.get({ key: this.HAS_LOGGED_IN }).then(({ value }) => {
      return value === 'true';
    });
  }
  async login() {
    await Storage.set({ key: this.HAS_LOGGED_IN, value: 'true' });
  }
  vioTutorial(): Promise<boolean> {
    return Storage.get({ key: this.VIO_TUTORIAL }).then(({ value }) => {
      return value === 'true';
    });
  }
  async setIdOnesignal(idOnesignal) {
    await Storage.set({
      key: this.PLAYER_ID_ONE_SIGNAL,
      value: JSON.stringify(idOnesignal),
    });
  }
  async getIdOnesignal() {
    await Storage.get({ key: this.PLAYER_ID_ONE_SIGNAL });
  }
  async setTutorialVisto() {
    await Storage.set({ key: this.VIO_TUTORIAL, value: 'true' });
  }
  logout() {
    this.presentAlert();
  }

  async presentAlert() {
    const alert = await this.alertCtrl.create({
      header: 'Atención',
      message: 'Está seguro que quiere cerrar sesión?',
      buttons: [
        {
          text: 'Cancelar',
          cssClass: 'secondary',
          handler: (blah) => {
            //
          },
        },
        {
          text: 'Aceptar',
          cssClass: 'primary',
          handler: () => {
            this.borrarDatosLogin();
            this.router.navigate(['/login', { replaceUrl: true }]);
          },
        },
      ],
    });

    await alert.present();
  }
  //USUARIO
  async setNumeroDNI(numeroDNI) {
    await Storage.set({
      key: this.NUMERO_DNI,
      value: JSON.stringify(numeroDNI),
    });
  }
  async setAvatar(data) {
    await Storage.set({
      key: this.AVATAR,
      value: JSON.stringify(data),
    });
  }
  async getAvatar() {
    return JSON.parse(await (await Storage.get({ key: this.AVATAR })).value);
  }
  async getNumeroDNI() {
    await Storage.get({ key: this.NUMERO_DNI });
  }
  async setIdUsuario(idUsuario) {
    await Storage.set({
      key: this.ID_USUARIO,
      value: JSON.stringify(idUsuario),
    });
  }
  async getIdUsuario() {
    await Storage.get({ key: this.ID_USUARIO });
  }
  async setToken(token) {
    this.token = token;
    await Storage.set({
      key: this.TOKEN,
      value: token,
    });
  }
  getToken() {
    if (this.token == '') {
      Storage.get({ key: this.TOKEN }).then((data) => {
        this.token = data.value;
        return data.value;
      });
    } else {
      return this.token;
    }
  }
  async setNickname(nickname) {
    await Storage.set({
      key: this.NICKNAME,
      value: nickname,
    });
  }
  async getNickname() {
    return await Storage.get({ key: this.NICKNAME });
  }
  async setFcmNombreUG(nombreUG) {
    await Storage.set({
      key: this.FCM_NOMBRE_UG,
      value: nombreUG,
    });
  }
  async getFcmNombreUG() {
    await Storage.get({ key: this.FCM_NOMBRE_UG });
  }
  async nombreKeys() {
    const listaKeys = await Storage.keys();
    listaKeys.keys.map(async (key) => {});
  }
  async borrarDatosLogin() {
    await Storage.remove({ key: this.NUMERO_DNI });
    await Storage.remove({ key: this.TOKEN });
    await Storage.set({ key: this.HAS_LOGGED_IN, value: 'false' });
  }

  //CLASES
  async setMisClase(idClase) {
    this.getMisClases();

    // this.misClases = value;
    this.misClases += 'clase' + idClase;
    await Storage.set({
      key: this.ID_CLASES_FAVORITAS,
      value: this.misClases,
    });
  }
  async getMisClases() {
    if (this.misClases == '') {
      return Storage.get({ key: this.ID_CLASES_FAVORITAS }).then((data) => {
        return data.value;
      });
    } else {
      return this.misClases;
    }
  }
  async borrarMisClases() {
    this.misClases = '';
    await Storage.set({ key: this.ID_CLASES_FAVORITAS, value: '' });
  }
}
// tengo una lista de idClase.
// El usuario puede agregar estas clases a sus clases.
// Tengo que agregar idClase a un array
