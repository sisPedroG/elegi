import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UserDataService } from '../user-data.service';
import { novedadInt, puntos, clase } from '../../utils/interfaces';
@Injectable({
  providedIn: 'root',
})
export class AppService {
  constructor(public http: HttpClient, public userData: UserDataService) {}
  public versionApp = '1.11';
  public servicioEndpoint =
    'https://www.dokoit.com/apps/bienestar/apiv1_4/servicios';
  token: string = '';

  login(numeroDNI) {
    let url: string = this.servicioEndpoint + '/login.php';
    let body = new FormData();
    body.append('numeroDNI', numeroDNI);
    body.append('versionApp', this.versionApp);
    return this.http
      .post(url, body)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }
  loginInvitado() {
    let url: string = this.servicioEndpoint + '/logininvitado.php';

    return this.http
      .get(url)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }
  async obtenerTalleres() {
    let url: string = this.servicioEndpoint + '/obtenertalleres.php';
    let body = new FormData();
    let token = this.obtenerToken();

    body.append('token', token);
    body.append('versionApp', this.versionApp);
    return this.http
      .post(url, body)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }
  async obtenerFaq() {
    let url: string = this.servicioEndpoint + '/obtenerfaq.php';
    let body = new FormData();
    let token = this.obtenerToken();

    body.append('token', token);
    body.append('versionApp', this.versionApp);
    return this.http
      .post(url, body)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }
  async obtenerEncuestas() {
    let url: string = this.servicioEndpoint + '/obtenerencuestas.php';
    let body = new FormData();
    let token = this.obtenerToken();

    body.append('token', token);
    body.append('versionApp', this.versionApp);
    return this.http
      .post(url, body)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }
  async obtenerPuntosAtributos() {
    let url: string = this.servicioEndpoint + '/obtenerpuntosatributos.php';
    let body = new FormData();
    let token = this.obtenerToken();
    body.append('token', token);
    body.append('versionApp', this.versionApp);
    return this.http
      .post(url, body)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }
  async enviarPuntosAtributos(puntos: puntos) {
    let url: string = this.servicioEndpoint + '/enviarpuntosatributos.php';
    let body = new FormData();
    let token = this.obtenerToken();
    body.append('token', token);
    body.append('versionApp', this.versionApp);
    body.append('destreza', String(puntos.destreza));
    body.append('conocimiento', String(puntos.conocimiento));
    body.append('creatividad', String(puntos.creatividad));
    body.append('bienestar', String(puntos.bienestar));

    return this.http
      .post(url, body)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }
  async enviarCodigo(codigo: string) {
    let url: string = this.servicioEndpoint + '/verificarcodigo.php';
    let body = new FormData();
    let token = this.obtenerToken();
    body.append('token', token);
    body.append('versionApp', this.versionApp);
    body.append('codigo', codigo);

    return this.http
      .post(url, body)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }
  async enviarPlayerId(playerId: string) {
    let url: string = this.servicioEndpoint + '/enviarplayerid.php';
    let body = new FormData();
    let token = this.obtenerToken();
    body.append('token', token);
    body.append('versionApp', this.versionApp);
    body.append('playerId', playerId);

    return this.http
      .post(url, body)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }
  async enviarNickname(nickname: string) {
    let url: string = this.servicioEndpoint + '/enviarnickname.php';
    let body = new FormData();
    let token = this.obtenerToken();
    body.append('token', token);
    body.append('versionApp', this.versionApp);
    body.append('nickname', nickname);

    return this.http
      .post(url, body)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }
  async enviarFeedback(feedback: string) {
    let url: string = this.servicioEndpoint + '/enviarfeedback.php';
    let body = new FormData();
    let token = this.obtenerToken();
    body.append('token', token);
    body.append('versionApp', this.versionApp);
    body.append('feedback', feedback);

    return this.http
      .post(url, body)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }
  async enviarInscripcionClase(idClase: number) {
    let url: string = this.servicioEndpoint + '/enviarinscripcionclase.php';
    let body = new FormData();
    let token = this.obtenerToken();
    body.append('token', token);
    body.append('versionApp', this.versionApp);
    body.append('idClase', String(idClase));

    return this.http
      .post(url, body)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }
  async enviarDesinscripcionClase(idClase: number) {
    let url: string = this.servicioEndpoint + '/enviardesinscripcionclase.php';
    let body = new FormData();
    let token = this.obtenerToken();
    body.append('token', token);
    body.append('versionApp', this.versionApp);
    body.append('idClase', String(idClase));

    return this.http
      .post(url, body)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }
  async marcarVideoVisto(idTallerVideos: number, idTaller: number) {
    let url: string = this.servicioEndpoint + '/marcarvideovisto.php';
    let body = new FormData();
    let token = this.obtenerToken();
    body.append('token', token);
    body.append('versionApp', this.versionApp);
    body.append('idTallerVideos', String(idTallerVideos));
    body.append('idTaller', String(idTaller));

    return this.http
      .post(url, body)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }
  async enviarAvatar(avatar: any) {
    let url: string = this.servicioEndpoint + '/enviaravatar.php';
    let body = new FormData();
    let token = this.obtenerToken();
    body.append('token', token);
    body.append('versionApp', this.versionApp);
    body.append('avatar', JSON.stringify(avatar));
    return this.http
      .post(url, body)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }
  async obtenerClases() {
    let url: string = this.servicioEndpoint + '/obtenerclases.php';
    let body = new FormData();
    let token = this.obtenerToken();
    body.append('token', token);
    body.append('versionApp', this.versionApp);
    return this.http.post<Array<clase>>(url, body);
  }
  async obtenerArticulos() {
    let url: string = this.servicioEndpoint + '/obtenerarticulos.php';
    let body = new FormData();
    let token = this.obtenerToken();
    body.append('token', token);
    body.append('versionApp', this.versionApp);
    return this.http
      .post(url, body)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }

  async trivia($idTrivia_pregunta, $idTrivia_opcion, $idTrivia): Promise<any> {
    let url: string = this.servicioEndpoint + '/trivias.php';

    let body = new FormData();
    let token = this.obtenerToken();
    body.append('token', token);
    body.append('versionApp', this.versionApp);
    body.append('idTrivia_pregunta', $idTrivia_pregunta);
    body.append('idTrivia_opcion', $idTrivia_opcion);
    body.append('idTrivia', $idTrivia);

    return this.http
      .post(url, body)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }
  public obtenerToken() {
    return this.userData.getToken();
  }
  private extractData(res: Response) {
    return res || {};
  }
  private handleError(res: Response | any) {
    return Promise.reject(res.message || res);
  }
}
