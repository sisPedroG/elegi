import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { taller } from '../utils/interfaces';
import { AppService } from './app/app.service';
import { UserDataService } from './user-data.service';
@Injectable({
  providedIn: 'root',
})
export class TalleresService {
  talleresData: Array<taller> = [];
  talleres: Array<taller> = [];
  talleresEnCurso: Array<taller> = [];
  errorConexion: boolean = true;
  // clasesEnVivo: boolean = false;
  constructor(
    public http: HttpClient,
    public userData: UserDataService,
    public service: AppService
  ) {}

  async obtenerTalleres() {
    (await this.obtenerListaTalleres()).subscribe((data) => {
      if (data != null) {
        this.errorConexion = false;
        this.talleresData = data['talleres'];
        this.ordenarTalleres();
      } else {
        this.errorConexion = true;
      }
    });
  }
  async obtenerListaTalleres() {
    let url: string = this.service.servicioEndpoint + '/obtenertalleres.php';
    let body = new FormData();
    let token = await this.service.obtenerToken();
    body.append('token', token);
    body.append('versionApp', this.service.versionApp);
    return this.http.post<Array<taller>>(url, body);
  }
  ordenarTalleres() {
    if (this.talleresData != undefined) {
      // let talleresTmp = this.talleresData;
      this.talleresData.forEach((taller, i) => {
        let enCurso = false;
        let videosVistos = 0;
        taller.videos.forEach((video) => {
          if (video.visto == 1) {
            enCurso = true;
            videosVistos++;
          }
        });
        taller.videosVistos = videosVistos;
        if (enCurso) {
          if (videosVistos < taller.videos.length) {
            this.talleresEnCurso.push(taller);
          } else {
            this.talleresData[this.talleresData.indexOf(taller)].completo = 1;
          }
        } else {
          // this.talleres.push(taller);
        }
      });

      // this.talleresEnCurso.forEach((taller) => {
      //   let tmpTaller = this.talleresData.find(
      //     (x) => x.idTaller == taller.idTaller
      //   );
      // this.talleresData.push( // patea al fondo
      //   this.talleresData.splice(this.talleresData.indexOf(tmpTaller), 1)[0]
      // );
      // });

      //Tengo el array con datos del taller
      //Necesito mover al final los talleres que no hayan sido comenzado

      // this.talleresData = talleresTmp;
    }
  }
  marcarVideoVisto(idTaller) {
    let tmpTaller = this.talleresEnCurso.find(
      (taller) => taller.idTaller == idTaller
    ); // creo una instancia del taller en talleresEnCurso para buscar el indice
    if (tmpTaller == undefined) {
      tmpTaller = this.talleresData.find(
        (taller) => taller.idTaller == idTaller
      );
      tmpTaller.videosVistos = 0;
      this.talleresEnCurso.push(tmpTaller);
    }
    let index = this.talleresEnCurso.indexOf(tmpTaller);
    this.talleresEnCurso[index].videosVistos++;
    if (
      this.talleresEnCurso[index].videosVistos >=
      this.talleresEnCurso[index].videos.length
    ) {
      this.talleresEnCurso.splice(index, 1);
      this.talleresData[this.talleresData.indexOf(tmpTaller)].completo = 1; //
    } else {
      this.talleresData[this.talleresData.indexOf(tmpTaller)].completo = 0; //
    }
  }
}
