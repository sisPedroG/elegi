import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { clase } from '../utils/interfaces';
import { AppService } from './app/app.service';
import { UserDataService } from './user-data.service';
// import format from 'date-fns/format';
import isAfter from 'date-fns/isAfter';
import isBefore from 'date-fns/isBefore';
import addMinutes from 'date-fns/addMinutes';

@Injectable({
  providedIn: 'root',
})
export class ClasesService {
  listaClases: Array<clase> = [];
  clasesData: Array<clase> = [];
  errorConexion: boolean = true;
  clasesEnVivo: boolean = false;
  public versionApp = '2.0';
  public servicioEndpoint =
    'https://www.dokoit.com/apps/bienestar/apiv1_0/servicios';

  constructor(
    public http: HttpClient,
    public userData: UserDataService,
    public service: AppService
  ) {
    // this.obtenerClases();
  }

  async obtenerClases(segment?: string) {
    if (segment == undefined) segment = 'misClases';
    (await this.obtenerListaClases()).subscribe((data) => {
      if (data != null) {
        this.errorConexion = false;
        this.clasesData = data;
        console.log(this.clasesData);
        this.filtrarClases(segment);
      } else {
        this.errorConexion = true;
      }
    });
  }
  async obtenerListaClases() {
    let url: string = this.servicioEndpoint + '/obtenerclases.php';
    let body = new FormData();
    let token = await this.service.obtenerToken();
    body.append('token', token);
    body.append('versionApp', this.versionApp);
    return this.http.post<Array<clase>>(url, body);
    // (await this.service.obtenerClases()).subscribe((data) => {
    //   this.clasesData = data;
    // });
    // return 0;
  }
  borrarseDeLaClase(idClase) {
    // this.clasesData.forEach(clase=>{
    //   clase.idClase==idClase?
    // })
    let tmpClase = this.clasesData.find((clase) => clase.idClase == idClase);
    if (tmpClase != undefined || tmpClase != null) {
      console.log(this.clasesData[this.clasesData.indexOf(tmpClase)]);
      this.clasesData[this.clasesData.indexOf(tmpClase)].inscripto = 0;
    }
    // if (this.clasesData.length == 0) {
    //   await this.obtenerClases();
    // }
    // this.clasesData[0].enVivo = true; // HARDCODED FOR TESTING
    // this.filtrarClases(s);
  }
  async actualizarListaClases(segment) {
    // if (this.clasesData.length == 0) {
    //   await this.obtenerClases();
    // }
    // this.clasesData[0].enVivo = true; // HARDCODED FOR TESTING
    this.filtrarClases(segment);
  }
  filtrarClases(segment) {
    this.listaClases = [];
    if (this.clasesData != undefined) {
      if (segment === 'inscribirse') {
        this.clasesData.filter((clase) => {
          Number(clase.inscripto) == 0 ? this.listaClases.push(clase) : 0;
        });
      } else if (segment === 'misClases') {
        this.clasesData.filter((clase) => {
          clase.enVivo = false;
          clase.enVivo = this.verificarClaseEnVivo(clase);
          if (Number(clase.inscripto) == 1) {
            this.listaClases.push(clase);
            console.log('true para', clase);
            clase.enVivo ? (this.clasesEnVivo = true) : 0;
          }
        });
      }
    }
  }
  verificarClaseEnVivo(clase: clase) {
    var fechaActual = new Date();
    var numeroDeDia = fechaActual.getDay();
    let estaSucendiendo: boolean = false;
    clase.horarios.forEach((horario) => {
      if (horario['dia'] == numeroDeDia) {
        var fechaClase = new Date();
        fechaClase.setHours(horario['horas']);
        fechaClase.setMinutes(horario['minutos']);
        fechaClase.setSeconds(0);
        if (
          isAfter(fechaActual, fechaClase) &&
          isBefore(fechaActual, addMinutes(fechaClase, horario['duracion']))
        ) {
          estaSucendiendo = true;
        }
      }
    });
    return estaSucendiendo;
  }
}
