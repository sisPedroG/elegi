import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { NivelesService } from '../providers/niveles.service';

@Component({
  selector: 'app-mis-misiones',
  templateUrl: './mis-misiones.page.html',
  styleUrls: ['./mis-misiones.page.scss'],
})
export class MisMisionesPage implements OnInit {
  constructor(public location: Location, public nivelesS: NivelesService) {}

  ngOnInit() {}
  volver() {
    this.location.back();
  }
}
