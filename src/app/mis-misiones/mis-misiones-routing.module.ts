import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MisMisionesPage } from './mis-misiones.page';

const routes: Routes = [
  {
    path: '',
    component: MisMisionesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MisMisionesPageRoutingModule {}
