import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MisMisionesPageRoutingModule } from './mis-misiones-routing.module';

import { MisMisionesPage } from './mis-misiones.page';
import { ComponentsModule } from '../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MisMisionesPageRoutingModule,
    ComponentsModule,
  ],
  declarations: [MisMisionesPage],
})
export class MisMisionesPageModule {}
