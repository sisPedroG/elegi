import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RoadtripVideoPage } from './roadtrip-video.page';

const routes: Routes = [
  {
    path: '',
    component: RoadtripVideoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RoadtripVideoPageRoutingModule {}
