import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { puntos, video } from '../utils/interfaces';
import { AppService } from '../providers/app/app.service';
import { Location } from '@angular/common';
import { ToastController } from '@ionic/angular';
import { NivelesService } from '../providers/niveles.service';
import { TalleresService } from '../providers/talleres.service';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';

@Component({
  selector: 'app-roadtrip-video',
  templateUrl: './roadtrip-video.page.html',
  styleUrls: ['./roadtrip-video.page.scss'],
})
export class RoadtripVideoPage implements OnInit {
  video: video;
  idTaller: number;
  safeURL: SafeResourceUrl;
  puntos: puntos;
  atributosUsuario: puntos;
  ocultarModal: boolean = true;
  constructor(
    public router: Router,
    private _sanitizer: DomSanitizer,
    public service: AppService,
    private location: Location, // public platform: Platform
    public nivelesService: NivelesService,
    public talleresService: TalleresService,
    public toastController: ToastController,
    public screenOrientation: ScreenOrientation
  ) {}

  async ngOnInit() {
    this.video = this.router.getCurrentNavigation().extras.state.video;
    this.idTaller = this.router.getCurrentNavigation().extras.state.idTaller;

    this.safeURL = this._sanitizer.bypassSecurityTrustResourceUrl(
      'https://youtube.com/embed/' + this.video.urlVideo
    );

    this.puntos = {
      destreza: Number(this.video['destreza']),
      creatividad: Number(this.video['creatividad']),
      conocimiento: Number(this.video['conocimiento']),
      bienestar: Number(this.video['bienestar']),
    };
    this.atributosUsuario = await this.service.obtenerPuntosAtributos();
    this.screenOrientation.unlock();
    // 2. This code loads the IFrame Player API code asynchronously.
  }

  ionViewDidEnter() {
    // this.platform.ready().then(() => {
    //   var tag = document.createElement('script');
    //   tag.src = 'https://www.youtube.com/iframe_api';
    //   var firstScriptTag = document.getElementById('scriptYTAPI');
    //   firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
    //   // 3. This function creates an <iframe> (and YouTube player)
    //   //    after the API code downloads.
    //   window['onYouTubePlayerAPIReady'] = () => {
    //     this.onYouTubePlayer();
    //   };
    // });
  }

  // onYouTubePlayer() {
  //   var player;

  //   player = window['YT'].Player('player', {
  //     height: '390',
  //     width: '640',
  //     videoId: 'M7lc1UVf-VE',
  //     events: {
  //       onReady: onPlayerReady,
  //       onStateChange: onPlayerStateChange,
  //     },
  //   });

  //   // 4. The API will call this function when the video player is ready.
  //   function onPlayerReady(event) {
  //     event.target.playVideo();
  //   }

  //   // 5. The API calls this function when the player's state changes.
  //   //    The function indicates that when playing a video (state=1),
  //   //    the player should play for six seconds and then stop.
  //   var done = false;
  //   function onPlayerStateChange(event) {
  //     if (event.data == window['YT'].PlayerState.PLAYING && !done) {
  //       setTimeout(stopVideo, 6000);
  //       done = true;
  //     }
  //   }
  //   function stopVideo() {
  //     player.stopVideo();
  //   }
  // }
  marcarVideoVisto() {
    this.video['visto'] = 1;
    this.talleresService.marcarVideoVisto(this.idTaller);
    this.service
      .marcarVideoVisto(this.video.idTallerVideos, this.idTaller)
      .then((data) => {
        // let xpAcumulada = 0;
        this.nivelesService.actualizarExperiencia(this.puntos);
        if (data == 'completo') {
          this.ocultarModal = false;
          this.presentToast('¡Has completado el curso!');
        } else {
          if (this.nivelesService.subeDeNivel(this.puntos)) {
            this.presentToast('¡Subiste de nivel!');
          }
        }
        if (data != 'marcado') {
          this.service.enviarPuntosAtributos(this.puntos).then((data) => {
            if (data == 'ok') {
              console.log('ta ok');
              // this.nivelesService.actualizarExperiencia(this.puntos); // lo puse en el local
              // this.nivelesService.
            }
          });
        }
      });
  }
  ocultarModalFn() {
    this.ocultarModal = true;
    this.puntos = {};
  }
  async presentToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 3000,
      color: 'success',
    });
    toast.present();
  }
  volver() {
    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
    this.location.back();
  }
}

// @ViewChild('videoIframe') videoIframe: any;
// onLoadEvent: Event;
// windowYT: any;
// player: any;
// done: boolean;
// show: boolean = false;
// this.loadYTAPI().then(() => {
//   this.show = false;
// });
// this.video = this.router.getCurrentNavigation().extras.state;
// this.safeURL = this._sanitizer.bypassSecurityTrustResourceUrl(
//   'https://youtube.com/embed/' + this.video.urlVideo + '?controls=0'
//   );
//   async loadYTAPI() {
//     var tag = document.createElement('script');
//     tag.id = 'scriptYT';
//     tag.src = 'https://www.youtube.com/iframe_api';
//     var firstScriptTag = document.getElementById('playerYT');
//     firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
//   }
//   onPlayerReady(event) {
//     event.target.playVideo();
//   }

//   onPlayerStateChange(event) {
//     if (event.data == window['YT'].PlayerState.PLAYING && !this.done) {
//       setTimeout(this.stopVideo, 6000);
//       this.done = true;
//     }
//   }
//   stopVideo() {
//     this.player.stopVideo();
//   }
//   onYouTubeIframeAPIReady() {
//     this.player = new window['YT'].Player('player', {
//       height: '390',
//       width: '640',
//       videoId: 'WWEXQRCddUs',
//       events: {
//         onReady: this.onPlayerReady,
//         onStateChange: this.onPlayerStateChange,
//       },
//     });
//   }
