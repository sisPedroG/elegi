import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RoadtripVideoPageRoutingModule } from './roadtrip-video-routing.module';

import { RoadtripVideoPage } from './roadtrip-video.page';
import { ComponentsModule } from '../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RoadtripVideoPageRoutingModule,
    ComponentsModule,
  ],
  declarations: [RoadtripVideoPage],
})
export class RoadtripVideoPageModule {}
