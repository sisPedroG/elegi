import { Location } from '@angular/common';
import {
  Component,
  OnInit,
  EventEmitter,
  Output,
  ViewChild,
  ElementRef,
} from '@angular/core';
import { Router } from '@angular/router';
import { AppService } from '../providers/app/app.service';
import { ClasesService } from '../providers/clases.service';
import { UserDataService } from '../providers/user-data.service';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { clase } from '../utils/interfaces';
import { AlertController } from '@ionic/angular';
@Component({
  selector: 'app-clase-detalle',
  templateUrl: './clase-detalle.page.html',
  styleUrls: ['./clase-detalle.page.scss'],
})
export class ClaseDetallePage implements OnInit {
  mostrarFormulario: boolean = false;
  constructor(
    private router: Router,
    public userData: UserDataService,
    public service: AppService,
    public location: Location,
    public claseService: ClasesService,
    private _sanitizer: DomSanitizer,
    public alertCtrl: AlertController
  ) {}
  @Output() actualizarLista = new EventEmitter<number>();
  clase: clase = {};
  safeURL: SafeResourceUrl;
  @ViewChild('encuesta') encuesta: ElementRef;

  async ngOnInit() {
    this.clase = this.router.getCurrentNavigation().extras.state;
    console.log(this.clase);
    if (this.clase.formulario != null || this.clase.formulario != undefined) {
      this.mostrarFormulario = true;
      this.safeURL = this._sanitizer.bypassSecurityTrustResourceUrl(
        this.clase.formulario
      );
    }
    // let misClases = await this.userData.getMisClases();
    // misClases.includes(String(this.clase.idClase))
    //   ? (this.clase.inscripto = true)
    //   : (this.clase.inscripto = false);
  }
  ngAfterViewInit() {
    console.log(this.encuesta);
  }
  desinscribirse() {
    console.log(this.encuesta);
    this.clase.inscripto = 0;

    this.service.enviarDesinscripcionClase(this.clase.idClase);
    this.claseService.filtrarClases('misClases');
    this.claseService.borrarseDeLaClase(this.clase.idClase);
  }
  async inscribirse() {
    // await this.userData.setMisClase(String(this.clase.idClase));
    this.service.enviarInscripcionClase(this.clase.idClase);
    this.claseService.filtrarClases('inscribirse');
    this.location.back();
  }
  ionViewWillLeave() {
    this.actualizarLista.emit();
  }
  volver() {
    this.location.back();
  }
  async presentAlert() {
    const alert = await this.alertCtrl.create({
      header: 'Inscripción',
      message: '¿Deseas inscribirte?',
      buttons: [
        {
          text: 'Cancelar',
          cssClass: 'secondary',
          handler: (blah) => {},
        },
        {
          text: 'Aceptar',
          cssClass: 'primary',
          handler: () => {
            this.irAFormulario();
          },
        },
      ],
    });

    await alert.present();
  }
  irAFormulario() {
    this.clase.inscripto = 1;

    let encuesta = {
      titulo: this.clase.titulo,
      formulario: this.clase.formulario,
      idClase: this.clase.idClase,
    };
    this.router.navigate(['encuesta-detalle'], { state: encuesta });
  }
}
