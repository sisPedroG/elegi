import { Location } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { IonInfiniteScroll } from '@ionic/angular';
import {
  AvatarOptions,
} from '../../projects/avatar/src/public-api';
import { RankingService } from '../providers/ranking.service';
@Component({
  selector: 'app-ranking',
  templateUrl: './ranking.page.html',
  styleUrls: ['./ranking.page.scss'],
})
export class RankingPage implements OnInit {
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  public options: AvatarOptions;
  avatar: AvatarOptions;
  categoria: string = 'general';
  constructor(public rankService:RankingService,
    public location:Location) {}

  ngOnInit() {
    const svgNode = document.getElementById('svgid');
    svgNode.innerHTML = ''; // FIX PARA EL AVATAR SUPERPUESTO
    this.avatar = new AvatarOptions();
    this.rankService.cargarRanking(this.categoria);
  }
  cambiarCategoria(categoria){
    this.categoria = categoria;
    console.log(categoria)
    this.rankService.cargarRanking(this.categoria);
    console.log(this.rankService)
  }
  cargarMasPuestos(){
    this.rankService.cargarMasPuestosRanking(this.categoria);
    this.infiniteScroll.complete();
  }
  volver(){
    this.location.back();
  }
}
