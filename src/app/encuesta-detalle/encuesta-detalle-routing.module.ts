import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EncuestaDetallePage } from './encuesta-detalle.page';

const routes: Routes = [
  {
    path: '',
    component: EncuestaDetallePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EncuestaDetallePageRoutingModule {}
