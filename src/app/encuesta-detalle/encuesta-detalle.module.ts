import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EncuestaDetallePageRoutingModule } from './encuesta-detalle-routing.module';

import { EncuestaDetallePage } from './encuesta-detalle.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EncuestaDetallePageRoutingModule
  ],
  declarations: [EncuestaDetallePage]
})
export class EncuestaDetallePageModule {}
