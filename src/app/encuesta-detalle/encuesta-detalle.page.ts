import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { AppService } from '../providers/app/app.service';
import { ClasesService } from '../providers/clases.service';

@Component({
  selector: 'app-encuesta-detalle',
  templateUrl: './encuesta-detalle.page.html',
  styleUrls: ['./encuesta-detalle.page.scss'],
})
export class EncuestaDetallePage implements OnInit {
  encuesta: any;
  safeURL: SafeResourceUrl;
  registroClase: boolean = false;

  constructor(
    public router: Router,
    private _sanitizer: DomSanitizer,
    public service: AppService,
    public classService: ClasesService
  ) {}

  ngOnInit() {
    this.encuesta = this.router.getCurrentNavigation().extras.state;
    this.safeURL = this._sanitizer.bypassSecurityTrustResourceUrl(
      this.encuesta.formulario
    );
    if (this.encuesta.idClase != null || this.encuesta.idClase != undefined) {
      this.registroClase = true;
    }
  }
  async inscribirse() {
    // await this.userData.setMisClase(String(this.clase.idClase));
    this.service.enviarInscripcionClase(this.encuesta.idClase);
    this.classService.filtrarClases('inscribirse');
  }
  volver() {
    if (this.registroClase) {
      this.inscribirse();
      this.router.navigate(['/tabs/clases']);
    } else {
      this.router.navigate(['/tabs/mas']);
    }
  }
}
